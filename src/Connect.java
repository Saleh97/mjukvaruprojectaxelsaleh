import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import java.util.ArrayList;


public class Connect {
    String username = "Alfa";
    String password = "Alfa123";
    String url = "jdbc:sqlserver://localhost;databaseName=mjukvaroUtveckling";

    // Hämtar ut BokningsID matchat med medskickat KundID
    public int showBookingID(int userID) {
        int bookingIDholder = 0;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            System.out.println("Connected");


            ResultSet rs = start.executeQuery("SELECT bookingID FROM BookingTable WHERE userID = '"+userID+"'");
            while (rs.next()) {
                bookingIDholder = rs.getInt("bookingID");
            }
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }

        return bookingIDholder;
    }

    // Lägger till kund i DB
    public void insertUser (String userName, String userPassword,String adress, String userKind){
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            start.execute("INSERT INTO UserTable VALUES ('"+userName+"','"+userPassword+"','"+adress+"','"+userKind+"')");
            System.out.println("Connected");
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Lägger till städare i DB
    public void insertCleaner (String userName, String userPassword,String adress){
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            start.execute("INSERT INTO CleanerTable VALUES ('"+userName+"','"+userPassword+"','"+adress+"')");
            System.out.println("Connected");
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Panel och metod för att visa alla Kunder i DB
    int removeUserByAdminUsingID;
    int removeUserIncreament = 0;
    static JPanel showUserTopList_TopPanel = new JPanel();
    static JPanel showUserList_Panel = new JPanel();
    ArrayList<Integer> removeUserByAdminList = new ArrayList<>();
    public void showUserList() {
        // Skapar scrollwheel och sätter properties

        Main.f.setLayout(new BorderLayout());
        showUserList_Panel.removeAll();
        showUserTopList_TopPanel.removeAll();
        showUserList_Panel.setLayout(new BoxLayout(showUserList_Panel, BoxLayout.Y_AXIS));
        RegisterAdmin.administrateUserMenu_Panel.setVisible(false);
        JScrollPane scrollPanee = new JScrollPane(showUserList_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPanee.setBounds(0,80,450,300);

        showUserTopList_TopPanel.setLayout(null);
        showUserTopList_TopPanel.setBounds(0,0,500,450);
        showUserTopList_TopPanel.setVisible(true);
        showUserTopList_TopPanel.add(scrollPanee);
        scrollPanee.setVisible(true);

        String name;
        String pass;
        String adress;
        String kind;

        // Try catch med sql injection till DB
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            System.out.println("Connected");

            ResultSet rs = start.executeQuery("SELECT * FROM UserTable");
            // Loopar igenom alla Kunder och skriver ut som en JLabel
            while (rs.next()) {
                removeUserByAdminUsingID = rs.getInt("userID");
                name = rs.getString("userName");
                pass = rs.getString("userPassword");
                adress = rs.getString("userAdress");
                kind = rs.getString("userKind");

                JLabel printUser = new JLabel("<html>--------------------------------------------------------------------------------------------------------------<br/>" +
                        "KundID: " + removeUserByAdminUsingID + "<br/>Namn: " + name + "<br/>Lösenord: " + pass +"<br/>Adress: " + adress + "<br/>Kundtyp: " + kind+"<br/><br/></html");
                showUserList_Panel.add(printUser);
                JButton[] removeUser = new JButton[removeUserIncreament+1];
                removeUser[removeUserIncreament] = new JButton("Radera kund");
                removeUser[removeUserIncreament].setBackground(Color.red);
                removeUser[removeUserIncreament].setForeground(Color.WHITE);

                // Metod för radera knappen, raderar kund med tillhörande KundID
                removeUser[removeUserIncreament].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int x = removeUserByAdminUsingID;

                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement start = con.createStatement()){
                            for (int z = 0; z<removeUser.length; z++){
                                if (e.getSource() == removeUser[z]){
                                    x = removeUserByAdminList.get(z);
                                    start.execute("DELETE FROM UserTable WHERE userID = " + x);
                                    System.out.println(x +"removed");
                                    showUserList_Panel.remove(removeUser[z]);
                                }
                            }

                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        showUserList_Panel.revalidate();
                        showUserList_Panel.repaint();

                    }
                });
                showUserList_Panel.add(removeUser[removeUserIncreament]);
                removeUserByAdminList.add(removeUserByAdminUsingID);
                removeUserIncreament++;
            }
        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }

        JLabel removeUser_label = new JLabel("Radera Kund");
        removeUser_label.setBounds(70,13,250,30);
        removeUser_label.setFont(new Font("Arial",Font.PLAIN,25));
        // Salehs
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axels
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,10,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                scrollPanee.setVisible(false);
                showUserTopList_TopPanel.setVisible(false);
                scrollPanee.removeAll();
                RegisterAdmin.administrateUserMenu_Panel.setVisible(true);
            }
        });
        showUserTopList_TopPanel.add(back_label);
        showUserTopList_TopPanel.add(removeUser_label);
    }



    // Metod och panel för att visa alla städare, likadan som showUserList
    static JPanel ShowCleanerTOPList_TopPanel = new JPanel();
    static JPanel showCleanerList_Panel = new JPanel();
    ArrayList<Integer> removeCleanerByAdminList = new ArrayList<>();
    int removeCleanerByAdminUsingID;
    int removeCleanerIncrement = 0;
    public void showCleanerList() {
        Main.f.setLayout(new BorderLayout());
        showCleanerList_Panel.removeAll();
        ShowCleanerTOPList_TopPanel.removeAll();
        showCleanerList_Panel.setLayout(new BoxLayout(showCleanerList_Panel, BoxLayout.Y_AXIS));
        RegisterAdmin.administrateCleanerMenu_Panel.setVisible(false);

        JScrollPane scrollPane = new JScrollPane(showCleanerList_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,450,300);
        ShowCleanerTOPList_TopPanel.setLayout(null);
        ShowCleanerTOPList_TopPanel.setBounds(0,0,500,450);
        ShowCleanerTOPList_TopPanel.setVisible(true);
        ShowCleanerTOPList_TopPanel.add(scrollPane);
        scrollPane.setVisible(true);

        String name;
        String pass;
        String adress;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            System.out.println("Connected");

            ResultSet rs = start.executeQuery("SELECT * FROM CleanerTable");
            while (rs.next()) {
                removeCleanerByAdminUsingID = rs.getInt("cleanerID");
                name = rs.getString("cleanerName");
                pass = rs.getString("cleanerPassword");
                adress = rs.getString("cleanerAdress");

                JLabel printCleaner = new JLabel("<html>----------------------------------------------------------------------------------------------------------------<br/>" +
                        "StädarID: " + removeCleanerByAdminUsingID + "<br/>Namn: " + name + "<br/>Lösenord: " + pass +"<br/>Adress: " + adress + "<br/><br/></html");
                showCleanerList_Panel.add(printCleaner);
                JButton[] removeCleaner = new JButton[removeCleanerIncrement+1];
                removeCleaner[removeCleanerIncrement] = new JButton("Radera städare");
                removeCleaner[removeCleanerIncrement].setBackground(Color.RED);
                removeCleaner[removeCleanerIncrement].setForeground(Color.WHITE);
                removeCleaner[removeCleanerIncrement].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int x = removeCleanerByAdminUsingID;

                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement start = con.createStatement()){
                            for (int z = 0; z<removeCleaner.length; z++){
                                if (e.getSource() == removeCleaner[z]){
                                    x = removeCleanerByAdminList.get(z);
                                    start.execute("DELETE FROM CleanerTable WHERE cleanerID = " + x);
                                    System.out.println(x +"removed");
                                    showCleanerList_Panel.remove(removeCleaner[z]);
                                }
                            }

                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        showCleanerList_Panel.revalidate();
                        showCleanerList_Panel.repaint();
                    }
                });
                showCleanerList_Panel.add(removeCleaner[removeCleanerIncrement]);
                removeCleanerByAdminList.add(removeCleanerByAdminUsingID);
                removeCleanerIncrement++;
            }

        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }

        JLabel removeCleaner_label = new JLabel("Radera Städare");
        removeCleaner_label.setBounds(70,13,250,30);
        removeCleaner_label.setFont(new Font("Arial",Font.PLAIN,25));
        // Salehs
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axels
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,10,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ShowCleanerTOPList_TopPanel.setVisible(false);
                scrollPane.setVisible(false);
                RegisterAdmin.administrateCleanerMenu_Panel.setVisible(true);
            }
        });
        ShowCleanerTOPList_TopPanel.add(back_label);
        ShowCleanerTOPList_TopPanel.add(removeCleaner_label);
    }

    boolean userConfrim = false;
    boolean userTrueInput = true;

    // Panel och metod för att visa alla städningar som är klara på något sätt, utfört, godkänd, underkänd o.s.v.
    static JPanel showAllDoneUserBookingTOP_TOPPanel = new JPanel();
    static JPanel showAllDoneUserBookings_Panel = new JPanel();
    int confirmid = 0;
    int l = 0;
    ArrayList<Integer> confirmListUser = new ArrayList<>();
    ArrayList<Integer> unconfirmListUser = new ArrayList<>();
    public void showAllDoneUserBookings(){
        Main.f.setLayout(new BorderLayout());
        showAllDoneUserBookings_Panel.removeAll();
        showAllDoneUserBookingTOP_TOPPanel.removeAll();

        showAllDoneUserBookings_Panel.setLayout(null);
        showAllDoneUserBookings_Panel.setBounds(0,80,500,400);
        showAllDoneUserBookings_Panel.setPreferredSize(new Dimension(500,400));
        Register.successful_customer_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(showAllDoneUserBookings_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,450,300);
        scrollPane.setViewportView(showAllDoneUserBookings_Panel);

        showAllDoneUserBookingTOP_TOPPanel.setLayout(null);
        showAllDoneUserBookingTOP_TOPPanel.setBounds(0,0,500,450);
        showAllDoneUserBookingTOP_TOPPanel.setVisible(true);
        showAllDoneUserBookingTOP_TOPPanel.add(scrollPane);
        scrollPane.setVisible(true);

        l = 0;
        userConfrim = false;
       userTrueInput = true;
        int numberOfY = 0;
        String datum;
        String status;
        String adress;
        String tid;
        String done = "Utfört";

        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            System.out.println("Connected");
            ResultSet rs = start.executeQuery("SELECT * FROM BookingTable  WHERE userID ='"+CreateBooking.getId()+"' AND bookingStatus ='"+done+"'" +
                    " OR userID ='"+CreateBooking.getId()+"' AND bookingStatus = 'Godkänt' OR userID ='"+CreateBooking.getId()+"' " +
                    "AND bookingStatus = 'Underkänt' OR userID ='"+CreateBooking.getId()+"' AND bookingStatus = 'Fakturerad' OR userID ='"+CreateBooking.getId()+"' AND bookingStatus = 'Betald' ORDER BY bookingID DESC");

            while (rs.next()) {
                JButton[] confirm = new JButton[l+1];
                JButton[] unconfirm = new JButton[l+1];
                confirmid = rs.getInt("bookingID");
                datum = rs.getString("bookingDate");
                status = rs.getString("bookingStatus");
                adress = rs.getString("adress");
                tid = rs.getString("bookingTime");

                JLabel printBooking = new JLabel("<html>BokningsID: " + confirmid + "<br/>Datum: " + datum + "<br/>Tid: " + tid +"<br/>Status: " + status + "<br/>Adress: " + adress+"<br/></html");
                printBooking.setBounds(5,numberOfY+10,150,100);
                confirm[l] = new JButton("Godkänn städning");
               confirm[l].setBounds(145,numberOfY +45,145,30);
               confirm[l].setBackground(Color.GREEN);
               unconfirm[l] = new JButton("Underkänn städning");
               unconfirm[l].setBounds(295,numberOfY+45,148,30);
               unconfirm[l].setBackground(Color.RED);

                JLabel lineBottom_label = new JLabel("----------------------------------------------------------------------------------------------------------------");
                lineBottom_label.setBounds(0,numberOfY+110,500,5);
                System.out.println(l);
                showAllDoneUserBookings_Panel.add(printBooking);
                showAllDoneUserBookings_Panel.add(lineBottom_label);
                showAllDoneUserBookings_Panel.setPreferredSize(new Dimension(500,101+numberOfY));

                numberOfY = numberOfY + 110;
                confirm[l].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int  x = confirmid;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement start = con.createStatement()){
                            for (int z = 0; z<confirm.length; z++){
                                System.out.println("Detta är värdet på z "+ z);
                                if (e.getSource() == confirm[z]){
                                    x = confirmListUser.get(z);
                                    showAllDoneUserBookings_Panel.remove(confirm[z]);
                                    showAllDoneUserBookings_Panel.remove(unconfirm[z]);
                                    start.execute("UPDATE BookingTable SET bookingStatus = 'Godkänt' WHERE bookingID = " + x);
                                }
                            }

                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        showAllDoneUserBookings_Panel.revalidate();
                        showAllDoneUserBookings_Panel.repaint();
                    }
                });
                unconfirm[l].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int  x = confirmid;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement start = con.createStatement()){
                            for (int z = 0; z<confirm.length; z++){
                                System.out.println("Detta är värdet på z "+ z);
                                if (e.getSource() == unconfirm[z]){
                                    x = unconfirmListUser.get(z);
                                    showAllDoneUserBookings_Panel.remove(confirm[z]);
                                    showAllDoneUserBookings_Panel.remove(unconfirm[z]);
                                    start.execute("UPDATE BookingTable SET bookingStatus = 'Underkänt' WHERE bookingID = " + x);
                                }
                            }
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                });
                if (status.trim().equals("Utfört")){
                    showAllDoneUserBookings_Panel.add(confirm[l]);
                    showAllDoneUserBookings_Panel.add(unconfirm[l]);
                }
                confirmListUser.add(confirmid);
                unconfirmListUser.add(confirmid);
                l++;
            }
            JLabel myDonebooking_label = new JLabel("Utförda Bokningar");
            myDonebooking_label.setBounds(70,13,250,30);
            myDonebooking_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);

            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    showAllDoneUserBookingTOP_TOPPanel.setVisible(false);
                    Register.successful_customer_loginPage.setVisible(true);
                }
            });
            showAllDoneUserBookingTOP_TOPPanel.add(back_label);
            showAllDoneUserBookingTOP_TOPPanel.add(myDonebooking_label);
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Panel och metod för att visa den inloggade kundens alla möjliga bokningar
    static JPanel showAllUserBookings_Panel = new JPanel();
    static JPanel showAllUserBookingTOP_TOPPanel = new JPanel();
    public void showAllUserBookings(){
        Main.f.setLayout(new BorderLayout());
        showAllUserBookings_Panel.removeAll();
        showAllUserBookingTOP_TOPPanel.removeAll();
        showAllUserBookings_Panel.setLayout(new BoxLayout(showAllUserBookings_Panel, BoxLayout.Y_AXIS));
        Register.successful_customer_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(showAllUserBookings_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        scrollPane.setBounds(0,80,400,300);
        showAllUserBookingTOP_TOPPanel.setLayout(null);
        showAllUserBookingTOP_TOPPanel.setBounds(0,0,500,450);
        showAllUserBookingTOP_TOPPanel.setVisible(true);

        showAllUserBookingTOP_TOPPanel.add(scrollPane);
        scrollPane.setVisible(true);

        int id;
        String datum;
        String status;
        String adress;
        String tid;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            System.out.println("Connected");

            ResultSet rs = start.executeQuery("SELECT * FROM BookingTable WHERE userID ='"+CreateBooking.getId()+"' AND bookingStatus = 'Obekräftad' OR userID ='"+CreateBooking.getId()+"' AND bookingStatus = 'Bekräftad'");
            while (rs.next()) {
                id = rs.getInt("bookingID");
                datum = rs.getString("bookingDate");
                status = rs.getString("bookingStatus");
                adress = rs.getString("adress");
                tid = rs.getString("bookingTime");

                JLabel printBooking = new JLabel("<html>---------------------------------<br/>BokningsID: " + id + "<br/>Datum: " + datum + "<br/>Tid: " + tid +"<br/>Status: " + status + "<br/>Adress: " + adress+"<br/></html");
                showAllUserBookings_Panel.add(printBooking);
            }

            JLabel mybooking_label = new JLabel("Mina Bokningar");
            mybooking_label.setBounds(70,13,250,30);
            mybooking_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
           // ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    showAllUserBookingTOP_TOPPanel.setVisible(false);
                    Register.successful_customer_loginPage.setVisible(true);
                }
            });
            showAllUserBookingTOP_TOPPanel.add(back_label);
            showAllUserBookingTOP_TOPPanel.add(mybooking_label);
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    int deleteuserincreament;
    int removeid = 0;
    String datum;
    String status;
    String adress;
    String tid;

    // Panel och metod för att kunden ska kunna radera en av sina obekräftade bokningar
    // Hämtar och visar alla bokningar som kan raderas
    static JPanel removeUserTOPBooking_TOPPanel = new JPanel();
    static JPanel removeUserBooking_Panel = new JPanel();
    ArrayList<Integer> removeBookingListByUser = new ArrayList<>();
    public void deleteBookingUser () {
        Main.f.setLayout(new BorderLayout());
        removeUserBooking_Panel.removeAll();
        removeUserTOPBooking_TOPPanel.removeAll();
        removeUserBooking_Panel.setLayout(new BoxLayout(removeUserBooking_Panel, BoxLayout.Y_AXIS));
        Register.successful_customer_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(removeUserBooking_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,450,300);

        removeUserTOPBooking_TOPPanel.setLayout(null);
        removeUserTOPBooking_TOPPanel.setBounds(0,0,500,450);
        removeUserTOPBooking_TOPPanel.setVisible(true);
        removeUserTOPBooking_TOPPanel.add(scrollPane);
        scrollPane.setVisible(true);
        deleteuserincreament = 0;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rs = start.executeQuery("SELECT * FROM BookingTable WHERE userID =" + CreateBooking.getId()+" AND bookingStatus = 'Obekräftad'");
            while (rs.next()){
                JButton[] remove = new JButton[deleteuserincreament+1];
                removeid = rs.getInt("bookingID");
                datum = rs.getString("bookingDate");
                status = rs.getString("bookingStatus");
                adress = rs.getString("adress");
                tid = rs.getString("bookingTime");
                remove[deleteuserincreament] = new JButton("Radera");
                remove[deleteuserincreament].setBackground(Color.RED);
                remove[deleteuserincreament].setForeground(Color.white);
                JLabel printBooking = new JLabel("<html>------------------------------------------------------------------------------------------------------------------------------------<br/>BokningsID: " + removeid + "<br/>Datum: " + datum + "<br/>Tid: " + tid +"<br/>Status: " + status + "<br/>Adress: " + adress+"<br/><br/></html");
                removeUserBooking_Panel.add(printBooking);

                remove[deleteuserincreament].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int  x3 = removeid;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement a = con.createStatement()){
                            for (int z = 0; z<remove.length; z++){
                                System.out.println("Detta är värdet på z "+ z);
                                if (e.getSource() == remove[z]){
                                    x = removeBookingListByUser.get(z);
                                    removeUserBooking_Panel.remove(remove[z]);
                                    a.execute("DELETE FROM BookingTable WHERE userID =" + CreateBooking.getId()+" AND bookingID ="+x3);
                                }
                            }
                        } catch (SQLException d) {
                            System.out.println("Kunde inte connecta");
                        }
                        removeUserBooking_Panel.revalidate();
                        removeUserBooking_Panel.repaint(40,15,150,500);
                    }
                });
                removeBookingListByUser.add(removeid);
                removeUserBooking_Panel.add(remove[deleteuserincreament]);
                deleteuserincreament++;
            }

            JLabel removebooking_label = new JLabel("Radera Bokning");
            removebooking_label.setBounds(70,13,250,30);
            removebooking_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);

            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    removeUserTOPBooking_TOPPanel.setVisible(false);
                    Register.successful_customer_loginPage.setVisible(true);
                }
            });
            removeUserTOPBooking_TOPPanel.add(back_label);
            removeUserTOPBooking_TOPPanel.add(removebooking_label);
        }
        catch (SQLException e){
            System.out.println("Kunde inte connecta");
        }
    }

    public void insertAdmin (String userName, String userPassword){
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            start.execute("INSERT INTO AdminTable VALUES ('"+userName+"','"+userPassword+"')");
            System.out.println("Connected");
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Panel och metod när admin ska redigera en bokning i DB
    static  JPanel administrate_TopBooking_TopPanel = new JPanel();
    static JPanel administrate_Booking_Panel = new JPanel();
    ArrayList<Integer> editBookingAdmin_button = new ArrayList<>();
    int editBookingIncreament = 0;
    int editID_Admin = 1;

    public void booking_Change_by_Admin(){
        Main.f.setLayout(new BorderLayout());
        administrate_Booking_Panel.removeAll();
        administrate_TopBooking_TopPanel.removeAll();
        administrate_Booking_Panel.setLayout(new BoxLayout(administrate_Booking_Panel, BoxLayout.Y_AXIS));
        RegisterAdmin.successful_Admin_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(administrate_Booking_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,450,300);

        administrate_TopBooking_TopPanel.setLayout(null);
        administrate_TopBooking_TopPanel.setBounds(0,0,500,450);
        administrate_TopBooking_TopPanel.setVisible(true);
        administrate_TopBooking_TopPanel.add(scrollPane);
        scrollPane.setVisible(true);
        editBookingAdmin_button.clear();
        editBookingIncreament = 0;

        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet allBokings = start.executeQuery("SELECT * FROM BookingTable");
            while (allBokings.next()){

                JButton[] edit = new JButton[editBookingIncreament +1];
                editID_Admin = allBokings.getInt("bookingID");
                String uID = allBokings.getString("userID");
                String cID = allBokings.getString("cleanerID");
                String bDate = allBokings.getString("bookingDate");
                String bTime = allBokings.getString("bookingTime");
                String bStatus = allBokings.getString("bookingStatus");
                String bKind = allBokings.getString("bookingKind");
                String bDescription = allBokings.getString("bookingDescription");
                JLabel printbooking = new JLabel("<html>.................................................................................................................................................<br/>BokningsID:" +editID_Admin+"<br/>KundID: "+uID+"<br/>StädareID: "+cID+"<br/>BokningsDatum: "+bDate+"<br/>BokningsTid: "+bTime+"<br/>BokningsStatus: "+bStatus+"<br/>BokningsTyp: "+bKind+"<br/>Bokningsbeskrivning: "+bDescription+"<br/><br/></html>");
                edit[editBookingIncreament] = new JButton("Redigera");
                edit[editBookingIncreament].setBackground(Color.ORANGE);
                administrate_Booking_Panel.add(printbooking);
                edit[editBookingIncreament].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int x = editID_Admin;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement a = con.createStatement()){
                            for (int z = 0; z<edit.length; z++){
                                if (e.getSource() == edit[z]){
                                    scrollPane.setVisible(false);
                                    editSpecificBooking_byAdmmin(editBookingAdmin_button.get(z));
                                }
                            }
                        } catch (SQLException d) {
                            System.out.println("Kunde inte connecta");
                        }
                        administrate_Booking_Panel.revalidate();
                        administrate_Booking_Panel.repaint();
                        administrate_TopBooking_TopPanel.revalidate();
                        administrate_TopBooking_TopPanel.repaint();
                        scrollPane.revalidate();
                        scrollPane.repaint();
                        scrollPane.setVisible(true);
                    }
                });
                editBookingAdmin_button.add(editID_Admin);
                administrate_Booking_Panel.add(edit[editBookingIncreament]);
                editBookingIncreament++;
            }
            JLabel removeUser_label = new JLabel("Administrera Bokningar");
            removeUser_label.setBounds(70,13,290,30);
            removeUser_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    administrate_TopBooking_TopPanel.setVisible(false);
                    scrollPane.setVisible(false);
                    RegisterAdmin.successful_Admin_loginPage.setVisible(true);
                }
            });
            administrate_TopBooking_TopPanel.add(back_label);
            administrate_TopBooking_TopPanel.add(removeUser_label);
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }


    // Panel och metod för sidan där admin väljer vad den ska redigera för den specifika bokningen
    static JPanel specificBooking_Panel = new JPanel();
    public void editSpecificBooking_byAdmmin(int bookingID){
        specificBooking_Panel.setLayout(null);
        specificBooking_Panel.removeAll();
        specificBooking_Panel.setBounds(0,10,500,450);
        administrate_Booking_Panel.setVisible(false);
        administrate_TopBooking_TopPanel.setVisible(false);
        specificBooking_Panel.setVisible(true);
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rs = start.executeQuery("SELECT * FROM BookingTable WHERE bookingID = "+bookingID);
            while (rs.next()){
                JLabel editbID_label = new JLabel("Redigerar -> BokningsID: "+ bookingID);
                editbID_label.setBounds(120,10,210,25);
                editbID_label.setFont(new Font("Arial",Font.PLAIN,15));

                JLabel uID_label = new JLabel("kundID");
                uID_label.setBounds(70,50,130,30);
                JTextField uID_textField = new JTextField(rs.getString("userID"));
                uID_textField.setEditable(false);
                uID_textField.setBounds(70,80,130,30);

                JLabel cID_label = new JLabel("StädareID");
                cID_label.setBounds(240,50,130,30);
                JTextField cID_textField = new JTextField(rs.getString("cleanerID"));
                System.out.println(rs.getInt("cleanerID"));
                cID_textField.setBounds(240,80,130,30);

                JLabel bDate_label = new JLabel("Datum");
                bDate_label.setBounds(70,120,130,30);
                JTextField bDate_textField = new JTextField(rs.getString("bookingDate").trim());
                bDate_textField.setBounds(70,150,130,30);

                JLabel bTime_label = new JLabel("Tid");
                bTime_label.setBounds(240,120,130,30);
                JTextField bTime_textField = new JTextField(rs.getString("bookingTime").trim());
                bTime_textField.setBounds(240,150,130,30);

                JLabel bAdress_label = new JLabel("Adress");
                bAdress_label.setBounds(70,190,130,30);
                JTextField bAdress_textField = new JTextField(rs.getString("adress").trim());
                bAdress_textField.setBounds(70,220,130,30);

                JLabel bStatus_label = new JLabel("Status");
                bStatus_label.setBounds(240,190,130,30);
                JTextField bStatus_textField = new JTextField(rs.getString("bookingStatus").trim());
                bStatus_textField.setBounds(240,220,130,30);

                JLabel bKind_label = new JLabel("BokningsTyp");
                bKind_label.setBounds(70,260,130,30);
                JTextField bKind_textField = new JTextField(rs.getString("bookingKind").trim());
                bKind_textField.setBounds(70,290,130,30);

                JLabel bDescription_label = new JLabel("Beskrivning");
                bDescription_label.setBounds(240,260,130,30);
                JTextField bDescription_textField = new JTextField(rs.getString("bookingDescription").trim());
                bDescription_textField.setBounds(240,290,130,30);

                JButton save_button = new JButton("Spara");
                save_button.setBounds(70,330,300,30);
                save_button.setBackground(Color.BLUE);
                save_button.setForeground(Color.WHITE);

                // Salehs
                ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
                // Axels
                //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");

                Image img = icon.getImage();
                Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
                icon = new ImageIcon(newimg);
                JLabel back_label = new JLabel(icon);
                back_label.setBounds(0,0,50,40);
                back_label.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        specificBooking_Panel.setVisible(false);
                        administrate_Booking_Panel.setVisible(true);
                        administrate_TopBooking_TopPanel.setVisible(true);
                    }
                });
                specificBooking_Panel.add(editbID_label);
                specificBooking_Panel.add(uID_label);
                specificBooking_Panel.add(uID_textField);
                specificBooking_Panel.add(cID_label);
                specificBooking_Panel.add(cID_textField);
                specificBooking_Panel.add(bDate_label);
                specificBooking_Panel.add(bDate_textField);
                specificBooking_Panel.add(bTime_label);
                specificBooking_Panel.add(bTime_textField);
                specificBooking_Panel.add(bAdress_label);
                specificBooking_Panel.add(bAdress_textField);
                specificBooking_Panel.add(bStatus_label);
                specificBooking_Panel.add(bStatus_textField);
                specificBooking_Panel.add(bKind_label);
                specificBooking_Panel.add(bKind_textField);
                specificBooking_Panel.add(bDescription_label);
                specificBooking_Panel.add(bDescription_textField);
                specificBooking_Panel.add(save_button);
                specificBooking_Panel.add(back_label);
                                save_button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement a = con.createStatement()){
                              a.execute("UPDATE BookingTable SET userID = "+uID_textField.getText()+", cleanerID = "+cID_textField.getText()+", bookingDate = '"+bDate_textField.getText()+"'" +
                                      ", bookingTime = '"+bTime_textField.getText()+"',adress = '"+bAdress_textField.getText()+"', bookingStatus = '"+bStatus_textField.getText()+"', bookingKind = '"+bKind_textField.getText()+"'" +
                                      ", bookingDescription = '"+bDescription_textField.getText()+"' WHERE bookingID = "+bookingID);
                            JOptionPane.showMessageDialog(null, "Ändringen för Bokningsnummer\n"+bookingID+" har sparats");
                        }catch (SQLException d) {
                            System.out.println("Kunde inte connecta");
                        }
                    }
                });
            }
        }catch (SQLException d) {
            System.out.println("Kunde inte connecta");
        }
    }


    // Metod för att kolla om det finns lediga städare vid den tiden som kunden skriver in
    // Finns det inga skrivs felmeddelande ut
    int bussy = 0;
    ArrayList <Integer> getIDOFClenaer = new ArrayList();
    public void checkBooking (int userID, int cleanerID, String bDate, String btime, String adress, String bDescription, String bStatus, String bookingKind){
        int i = 0;
        int cc = 0;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rss = start.executeQuery("SELECT * FROM CleanerTable");
            while (rss.next()){
                i = rss.getRow();
                getIDOFClenaer.add(rss.getInt("cleanerID"));
            }

            for (int p = 0; p<i; p++){
                cc = getIDOFClenaer.get(p);
                System.out.println(cc);

                bussy = 0;
                ResultSet c = start.executeQuery("SELECT * FROM BookingTable WHERE cleanerID = "+cc+" AND bookingDate= '"+bDate+"' AND bookingTime = '"+btime+"'");
                while (c.next()) {
                    bussy++;
                }
                if (bussy == 0){
                    cleanerID= cc;
                    start.execute("INSERT INTO BookingTable VALUES ('"+userID+"','"+cleanerID+"','"+bDate+"','"+btime+"','"+adress+"','"+bDescription+"','"+bStatus+"','"+bookingKind+"')");
                    x = i+1;
                    p = i+1;
                    JOptionPane.showMessageDialog(null, "Bokningen Skapades");
                }
            }
            if (bussy > 0){
                JOptionPane.showMessageDialog(null,"Inga lediga städare\n välj annan tid.");
            }

            System.out.println("Connected");
        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Metod för att kolla om kunden finns i DB
    int checkUser = 0;
    public void checkLogInUser(String name, String pass){
        checkUser = 0;
        Register register = new Register();
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rs = start.executeQuery("SELECT * FROM UserTable WHERE userName='"+name+"' AND userPassword='"+pass+"'");
            while (rs.next()) {
                checkUser++;
                int userID = rs.getInt("userID");
                CreateBooking.setId(userID);
            }
            if (checkUser >0){
                register.successful_login(name,pass);
                register.successful_customer_loginPage.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Fel namn eller lösenord");
            }
        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Metod för att kolla om städaren finns i DB
    int checkCleaner = 0;
    public void checkLogInCleaner(String name, String pass){
        RegisterCleaner cleaner = new RegisterCleaner();
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rs = start.executeQuery("SELECT * FROM CleanerTable WHERE cleanerName='"+name+"' AND cleanerPassword='"+pass+"'");
            while (rs.next()) {
                checkCleaner++;
            }
            if (checkCleaner >0){
               cleaner.successful_login_Cleaner(name,pass);
               RegisterCleaner.successful_cleaner_loginPage.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Fel namn eller lösenord");
            }
        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    int cleanerID = 1;
    boolean confirmBooking = false;
    boolean unconfirmed = true;


    // Panel och metod för att visa alla bokningar en städare har blivit tilldelad, men inte bekräftat
    static JPanel cleanerAssignedBookingPanel = new JPanel();
    static JPanel cleanerAssignedBookingTOP_TOPPanel = new JPanel();
    int y = 0;
    int confirmidCleanerMethod = 0;
    ArrayList<Integer> confirmListCleaner = new ArrayList<>();
    ArrayList<Integer> unconfirmListCleaner = new ArrayList<>();
    String datumChange;
    String timeChange;
    int cleanerChange;
    public void cleanerShowAllMyAssignedBooking (String cleanerN, String cleanerP){
        Main.f.setLayout(new BorderLayout());
        cleanerAssignedBookingPanel.removeAll();
        cleanerAssignedBookingTOP_TOPPanel.removeAll();
        cleanerAssignedBookingPanel.setLayout(null);
        //cleanerAssignedBookingPanel.setBounds(0,80,500,400);
        //cleanerAssignedBookingPanel.setPreferredSize(new Dimension(500,400));

        RegisterCleaner.successful_cleaner_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(cleanerAssignedBookingPanel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,470,300);
        scrollPane.setViewportView(cleanerAssignedBookingPanel);
        cleanerAssignedBookingTOP_TOPPanel.setLayout(null);
        cleanerAssignedBookingTOP_TOPPanel.setBounds(0,0,500,450);
        cleanerAssignedBookingTOP_TOPPanel.setVisible(true);
        cleanerAssignedBookingTOP_TOPPanel.add(scrollPane);
        scrollPane.setVisible(true);
        y = 0;
        confirmBooking = false;
        unconfirmed = true;

        int numberOfY = -10;
        String datum;
        String status;
        String adress;
        String tid;
        String typ;
        String beskrivning;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet myID = start.executeQuery("SELECT * FROM CleanerTable WHERE cleanerName = '"+cleanerN+"' AND cleanerPassword = '"+cleanerP+"'");
            while (myID.next()){
                 cleanerID = myID.getInt("cleanerID");
            }
            ResultSet allMyBooking = start.executeQuery("SELECT * FROM BookingTable WHERE cleanerID = "+cleanerID + " AND bookingStatus = 'Obekräftad'");
            while (allMyBooking.next()){
                JButton[] confirm = new JButton[y+1];
                JButton[] unconfirm = new JButton[y+1];
                confirmBooking = true;
                confirmidCleanerMethod = allMyBooking.getInt("bookingID");
                datum = allMyBooking.getString("bookingDate");
                tid = allMyBooking.getString("bookingTime");
                status = allMyBooking.getString("bookingStatus");
                typ = allMyBooking.getString("bookingKind");
                adress = allMyBooking.getString("adress");
                beskrivning = allMyBooking.getString("bookingDescription");

                JLabel printBooking = new JLabel("<html><br/>BokningsID: " + confirmidCleanerMethod + "<br/>Datum: " + datum + "<br/>Tid: " + tid +"<br/>Status: " + status + "<br/>Typ: "+ typ + "<br/>Adress: " + adress+"<br/>Beskrivning: "+ beskrivning +"</html>");
                printBooking.setBounds(5,numberOfY+10,150,150);
                confirm[y] = new JButton("Bekräfta");
                confirm[y].setBounds(145,numberOfY +45,145,30);
                confirm[y].setBackground(Color.GREEN);
                unconfirm[y] = new JButton("Obekräfta");
                unconfirm[y].setBounds(295,numberOfY+45,148,30);
                unconfirm[y].setBackground(Color.RED);
                JLabel lineBottom_label = new JLabel("----------------------------------------------------------------------------------------------------------------");
                lineBottom_label.setBounds(0,numberOfY+160,500,5);
                System.out.println(y);
                cleanerAssignedBookingPanel.add(lineBottom_label);
                cleanerAssignedBookingPanel.add(printBooking);
                cleanerAssignedBookingPanel.setPreferredSize(new Dimension(500,160+numberOfY));

                numberOfY = numberOfY + 160;
                confirm[y].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int  xb = confirmidCleanerMethod;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement start = con.createStatement()){
                            for (int z = 0; z<confirm.length; z++){
                                System.out.println("Detta är värdet på z "+ z);
                                if (e.getSource() == confirm[z]){
                                    xb = confirmListCleaner.get(z);
                                    cleanerAssignedBookingPanel.remove(confirm[z]);
                                    cleanerAssignedBookingPanel.remove(unconfirm[z]);
                                    start.execute("UPDATE BookingTable SET bookingStatus = 'Bekräftad' WHERE bookingID = " + xb);
                                }
                            }
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        cleanerAssignedBookingPanel.revalidate();
                        cleanerAssignedBookingPanel.repaint();
                    }
                });
                unconfirm[y].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int  x = confirmidCleanerMethod;
                        int i = 0;
                        int cc = 0;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement start = con.createStatement()){
                            ResultSet getDateAndTime = start.executeQuery("SELECT * FROM BookingTable WHERE bookingID = " + x);
                            while(getDateAndTime.next()){
                                datumChange = getDateAndTime.getString("bookingDate").trim();
                                timeChange = getDateAndTime.getString("bookingTime").trim();
                                cleanerChange = getDateAndTime.getInt("cleanerID");
                                System.out.println(datumChange +" " + timeChange);
                            }

                            for (int z = 0; z<confirm.length; z++){
                                System.out.println("Detta är värdet på z "+ z);
                                if (e.getSource() == unconfirm[z]){
                                    x = unconfirmListCleaner.get(z);
                                    System.out.println("Detta är värdet på X "+x);
                                    cleanerAssignedBookingPanel.remove(confirm[z]);
                                    cleanerAssignedBookingPanel.remove(unconfirm[z]);
                                    System.out.println(unconfirmListUser);
                                   // start.execute("UPDATE BookingTable SET bookingStatus = 'Obekräftad' WHERE bookingID = " + x);
                                    ResultSet rss = start.executeQuery("SELECT * FROM CleanerTable");
                                    while (rss.next()){
                                        i = rss.getRow();
                                        getIDOFClenaer.add(rss.getInt("cleanerID"));
                                    }

                                    for (int p = 0; p<i; p++){
                                        cc = getIDOFClenaer.get(p);
                                        System.out.println(cc);

                                        bussy = 0;
                                        ResultSet c = start.executeQuery("SELECT * FROM BookingTable WHERE cleanerID = "+cc+" AND bookingDate= '"+datumChange+"' AND bookingTime = '"+timeChange+"'");
                                        while (c.next()) {
                                            bussy++;
                                        }
                                        if (bussy == 0){
                                            if (cleanerChange == cc){
                                                System.out.println("Samma cleaner");
                                            }else {
                                                cleanerID = cc;
                                                //start.execute("INSERT INTO BookingTable VALUES ('"+userID+"','"+cleanerID+"','"+bDate+"','"+btime+"','"+adress+"','"+bDescription+"','"+bStatus+"','"+bookingKind+"')");
                                                start.execute("UPDATE BookingTable SET cleanerID = '" + cc + "' WHERE bookingID = " + x);
                                                // x = i+1;
                                                p = i + 1;
                                                //JOptionPane.showMessageDialog(null, "Bokningen Skapades");
                                            }
                                        }
                                    }
                                    if (bussy > 0){
                                        System.out.println("Bussy är 0");
                                       // start.execute("UPDATE BookingTable SET cleanerID = '" + cc + "' WHERE bookingID = " + x);
                                    }
                                }
                            }
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        cleanerAssignedBookingPanel.revalidate();
                        cleanerAssignedBookingPanel.repaint();
                    }
                });
                if (status.trim().equals("Obekräftad")){
                    cleanerAssignedBookingPanel.add(confirm[y]);
                    cleanerAssignedBookingPanel.add(unconfirm[y]);
                }
                confirmListCleaner.add(confirmidCleanerMethod);
                unconfirmListCleaner.add(confirmidCleanerMethod);
                y++;
            }
            JLabel mybooking_label = new JLabel("Tilldelade Städningar");
            mybooking_label.setBounds(70,13,250,30);
            mybooking_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    cleanerAssignedBookingTOP_TOPPanel.setVisible(false);
                    RegisterCleaner.successful_cleaner_loginPage.setVisible(true);
                }
            });
            cleanerAssignedBookingTOP_TOPPanel.add(back_label);
            cleanerAssignedBookingTOP_TOPPanel.add(mybooking_label);
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }


    // Panel och metod för att visa en städares alla bekräftade bokningar
    boolean doneBooking=false;
    boolean unDone = true;
    static JPanel cleanerConfirmedBookingsPanel = new JPanel();
    static JPanel cleanerConfirmedTOPBooking_TOPPanel = new JPanel();
    int x = 0;
    int bIDShowConfirmedBooking = 0;
    ArrayList<Integer> doneBookingList = new ArrayList<>();
    public void cleanerShowAllConfirmedBooking (String cleanerN, String cleanerP){
        Main.f.setLayout(new BorderLayout());
        cleanerConfirmedBookingsPanel.removeAll();
        cleanerConfirmedTOPBooking_TOPPanel.removeAll();
        cleanerConfirmedBookingsPanel.setLayout(new BoxLayout(cleanerConfirmedBookingsPanel, BoxLayout.Y_AXIS));
        RegisterCleaner.successful_cleaner_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(cleanerConfirmedBookingsPanel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,450,300);
        cleanerConfirmedTOPBooking_TOPPanel.setLayout(null);
        cleanerConfirmedTOPBooking_TOPPanel.setBounds(0,0,500,450);
        cleanerConfirmedTOPBooking_TOPPanel.setVisible(true);
        cleanerConfirmedTOPBooking_TOPPanel.add(scrollPane);
        scrollPane.setVisible(true);

        x = 0;
        String datum;
        String status;
        String adress;
        String tid;
        String beskrivning;
        unDone = true;
        doneBooking =false;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet myID = start.executeQuery("SELECT * FROM CleanerTable WHERE cleanerName = '"+cleanerN+"' AND cleanerPassword = '"+cleanerP+"'");
            while (myID.next()){
                cleanerID = myID.getInt("cleanerID");
            }
            ResultSet allMyBooking = start.executeQuery("SELECT * FROM BookingTable WHERE cleanerID = "+cleanerID + " AND bookingStatus = 'Bekräftad'");
            while (allMyBooking.next()){
                // Knapp för att sätta utfört på bokningen
                JButton[] done = new JButton[x+1];
                bIDShowConfirmedBooking = allMyBooking.getInt("bookingID");
                datum = allMyBooking.getString("bookingDate");
                status = allMyBooking.getString("bookingStatus");
                adress = allMyBooking.getString("adress");
                tid = allMyBooking.getString("bookingTime");
                beskrivning = allMyBooking.getString("bookingDescription");
                done[x] = new JButton("Utförd");
                done[x].setBackground(Color.GREEN);
                JLabel printBooking = new JLabel("<html>----------------------------------------------------------------------------------------------------------------<br/>BokningsID: " +
                        bIDShowConfirmedBooking + "<br/>Datum: " + datum + "<br/>Tid: " + tid +"<br/>Status: " + status + "<br/>Adress: " + adress+"<br/>Beskrivning: "+ beskrivning +"<br/><br/></html");
                cleanerConfirmedBookingsPanel.add(printBooking);

                done[x].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int x2 = bIDShowConfirmedBooking;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement start = con.createStatement()){
                            for (int z = 0; z<done.length; z++){
                                System.out.println("Detta är värdet på z "+ z);
                                if (e.getSource() == done[z]){
                                    x = doneBookingList.get(z);
                                    cleanerConfirmedBookingsPanel.remove(done[z]);
                                    start.execute("UPDATE BookingTable SET bookingStatus = 'Utfört' WHERE bookingID = " + x);
                                }
                            }
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                        cleanerConfirmedBookingsPanel.revalidate();
                        cleanerConfirmedBookingsPanel.repaint(40, 15, 150, 500);
                    }
                });
                cleanerConfirmedBookingsPanel.add(done[x]);
                doneBookingList.add(bIDShowConfirmedBooking);
                x++;
            }
            JLabel finishedbooking_label = new JLabel("Bekräftade Bokningar");
            finishedbooking_label.setBounds(70,13,250,30);
            finishedbooking_label.setFont(new Font("Arial",Font.PLAIN,25));

            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);

            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    cleanerConfirmedTOPBooking_TOPPanel.setVisible(false);
                    RegisterCleaner.successful_cleaner_loginPage.setVisible(true);
                }
            });
            cleanerConfirmedTOPBooking_TOPPanel.add(back_label);
            cleanerConfirmedTOPBooking_TOPPanel.add(finishedbooking_label);
        }
        catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Panel och metod som visar en städares utförda städningar
    boolean confirmDoneBooking=false;
    boolean ConfirmunDone = true;
    static JPanel cleanerDoneBookingsPanel = new JPanel();
    static JPanel cleanerDoneBookingsTOP_TOPPanel = new JPanel();
    public void cleanerShowAllDoneBooking (String cleanerN, String cleanerP){
        Main.f.setLayout(new BorderLayout());
        cleanerDoneBookingsPanel.removeAll();
        cleanerDoneBookingsTOP_TOPPanel.removeAll();
        cleanerDoneBookingsPanel.setLayout(new BoxLayout(cleanerDoneBookingsPanel, BoxLayout.Y_AXIS));
        RegisterCleaner.successful_cleaner_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(cleanerDoneBookingsPanel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,400,300);

        cleanerDoneBookingsTOP_TOPPanel.setLayout(null);
        cleanerDoneBookingsTOP_TOPPanel.setBounds(0,0,500,450);
        cleanerDoneBookingsTOP_TOPPanel.setVisible(true);
        cleanerDoneBookingsTOP_TOPPanel.add(scrollPane);
        scrollPane.setVisible(true);
        ConfirmunDone = true;
        confirmDoneBooking =false;

        int id;
        String datum;
        String status;
        String adress;
        String tid;
        String beskrivning;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet myID = start.executeQuery("SELECT * FROM CleanerTable WHERE cleanerName = '"+cleanerN+"' AND cleanerPassword = '"+cleanerP+"'");
            while (myID.next()){
                cleanerID = myID.getInt("cleanerID");
            }
            ResultSet allMyBooking = start.executeQuery("SELECT * FROM BookingTable WHERE cleanerID = "+cleanerID + " AND bookingStatus = 'Utfört' " +
                    "OR cleanerID = "+cleanerID + " AND bookingStatus = 'Godkänt' OR cleanerID = "+cleanerID + " AND bookingStatus = 'Underkänt'");
            while (allMyBooking.next()){
                doneBooking = true;
                id = allMyBooking.getInt("bookingID");
                datum = allMyBooking.getString("bookingDate");
                tid = allMyBooking.getString("bookingTime");
                status = allMyBooking.getString("bookingStatus");
                adress = allMyBooking.getString("adress");
                beskrivning = allMyBooking.getString("bookingDescription");
                JLabel printBooking = new JLabel("<html>--------------------------------------------------------------------------------------------------------------<br/>" +
                        "BokningsID: " + id + "<br/>Datum: " + datum + "<br/>Tid: " + tid +"<br/>Status: " + status + "<br/>Adress: " + adress+"<br/>Beskrivning: "+beskrivning+"</html>");
                cleanerDoneBookingsPanel.add(printBooking);
            }
            JLabel mybooking_label = new JLabel("Genomförda Bokningar");
            mybooking_label.setBounds(70,13,290,30);
            mybooking_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    cleanerDoneBookingsTOP_TOPPanel.setVisible(false);
                    RegisterCleaner.successful_cleaner_loginPage.setVisible(true);
                }
            });
            cleanerDoneBookingsTOP_TOPPanel.add(back_label);
            cleanerDoneBookingsTOP_TOPPanel.add(mybooking_label);
        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Kollar om admin som loggar in finns i DB
    int checkAdmin = 0;
    public void checkLogInAdmin(String name, String pass){
        RegisterAdmin admin = new RegisterAdmin();
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rs = start.executeQuery("SELECT * FROM AdminTable WHERE adminName='"+name+"' AND adminPassword='"+pass+"'");
            while (rs.next()) {
                checkAdmin++;
            }
            if (checkAdmin >0){
                admin.successful_login_Admin(name,pass);
            } else {
                JOptionPane.showMessageDialog(null, "Fel namn eller lösenord");
            }
        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Panel och metod för mina sidor sidan
    // Parametern tar emot vilken typ av användare det är
    static JPanel userMyPage_panel = new JPanel();
    public void myPageInfoUser(String name, String pass, String userkind){
        userMyPage_panel.setLayout(null);
        userMyPage_panel.setBounds(0,10,500,450);
        Register.successful_customer_loginPage.setVisible(false);
        RegisterCleaner.successful_cleaner_loginPage.setVisible(false);
        RegisterAdmin.successful_Admin_loginPage.setVisible(false);
        userMyPage_panel.setVisible(true);

        String  userInfo = "funkar inte";
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rs = start.executeQuery("SELECT * FROM UserTable WHERE userName='"+name+"' AND userPassword='"+pass+"'");
           if(userkind == "user") {
               while (rs.next()) {
                   userInfo = rs.getString("userAdress");
               }
           }
            else if (userkind == "cleaner") {
               ResultSet rsCleaner = start.executeQuery("SELECT * FROM CleanerTable WHERE cleanerName='" + name + "' AND cleanerPassword='" + pass + "'");
               while (rsCleaner.next()) {
                   userInfo = rsCleaner.getString("cleanerAdress");
               }
           }

            JLabel myPageinfo = new JLabel("Mina sidor");
            myPageinfo.setBounds(200,50,95,30);
            myPageinfo.setFont(new Font("Arial",Font.PLAIN,20));

            ImageIcon nameIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\userName.png");
            Image nameIcon_Image = nameIcon.getImage();
            Image newNameIconImage = nameIcon_Image.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
            nameIcon = new ImageIcon(newNameIconImage);
            JLabel usernameImage = new JLabel(nameIcon);
            usernameImage.setBounds(170,100,30,30);

            JLabel myUsername = new JLabel(name);
            myUsername.setBounds(220,100,130,30);

            ImageIcon passwordIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\userPassword.png");
            Image passwordIcon_Image = passwordIcon.getImage();
            Image newPasswordIconImage = passwordIcon_Image.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
            passwordIcon = new ImageIcon(newPasswordIconImage);
            JLabel userPasswordImage = new JLabel(passwordIcon);
            userPasswordImage.setBounds(170,150,30,30);

            JLabel myPassword = new JLabel(pass);
            myPassword.setBounds(220,150,130,30);

            ImageIcon adressIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\userAdress.png");
            Image adressIcon_Image = adressIcon.getImage();
            Image newadressIconImage = adressIcon_Image.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
            adressIcon = new ImageIcon(newadressIconImage);
            JLabel useradressImage = new JLabel(adressIcon);
            useradressImage.setBounds(170,200,30,30);

            JLabel myAdress = new JLabel(userInfo.trim());
            myAdress.setBounds(220,200,150,30);

            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);

            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,0,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (userkind == "user") {
                        userMyPage_panel.setVisible(false);
                        Register.successful_customer_loginPage.setVisible(true);
                    }
                    else if (userkind =="cleaner"){
                        userMyPage_panel.setVisible(false);
                        RegisterCleaner.successful_cleaner_loginPage.setVisible(true);
                    }
                    else if(userkind == "admin"){
                        userMyPage_panel.setVisible(false);
                        RegisterAdmin.successful_Admin_loginPage.setVisible(true);
                    }
                }
            });

            userMyPage_panel.removeAll();
            userMyPage_panel.add(myPageinfo);
            userMyPage_panel.add(myUsername);
            userMyPage_panel.add(myPassword);
            userMyPage_panel.add(usernameImage);
            userMyPage_panel.add(userPasswordImage);
            if (userkind != "admin") {
                userMyPage_panel.add(myAdress);
                userMyPage_panel.add(useradressImage);
            }
            userMyPage_panel.add(back_label);

        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    boolean isThereInvoice = false;
    // Panel och metod för att skicka faktura för en bokning
    static JPanel adminInvoiceTop_TopPanel = new JPanel();
    static JPanel adminInvoice_Panel = new JPanel();
    ArrayList<Integer> sendInvoice_button = new ArrayList<>();
    int sendInvoiceIncrement = 0;
    int sendInvoiceID_Admin = 1;
    String amount;
    public void adminSendInvoice(){
        Main.f.setLayout(new BorderLayout());
        adminInvoice_Panel.removeAll();
        adminInvoiceTop_TopPanel.removeAll();
        sendInvoice_button.clear();
        sendInvoiceIncrement = 0;
        adminInvoice_Panel.setBounds(0,80,500,400);
        RegisterAdmin.successful_Admin_loginPage.setVisible(false);
        adminInvoice_Panel.setVisible(true);

        adminInvoice_Panel.setLayout(new BoxLayout(adminInvoice_Panel, BoxLayout.Y_AXIS));
        JScrollPane scrollPane = new JScrollPane(adminInvoice_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,80,400,300);
        adminInvoiceTop_TopPanel.setLayout(null);
        adminInvoiceTop_TopPanel.setBounds(0,0,500,450);
        adminInvoiceTop_TopPanel.setVisible(true);
        adminInvoiceTop_TopPanel.add(scrollPane);
        scrollPane.setVisible(true);

        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {
            ResultSet rs = start.executeQuery("SELECT * FROM BookingTable WHERE bookingStatus = 'Godkänt' OR bookingStatus = 'Underkänt'");
            while (rs.next()) {

                JButton[] sendInvoice = new JButton[sendInvoiceIncrement+1];
                isThereInvoice = true;
                sendInvoiceID_Admin = rs.getInt("bookingID");
                String uID = rs.getString("userID");
                String cID = rs.getString("cleanerID");
                String bDate = rs.getString("bookingDate");
                String bTime = rs.getString("bookingTime");
                String bStatus = rs.getString("bookingStatus");
                String bKind = rs.getString("bookingKind");
                String bDescription = rs.getString("bookingDescription");
                JLabel printbooking = new JLabel("<html>...............................................................................................................<br/>BokningsID:" +sendInvoiceID_Admin+"<br/>KundID: "+uID+"<br/>StädareID: "+cID+"<br/>BokningsDatum: "+bDate+"<br/>" +
                        "BokningsTid: "+bTime+"<br/>BokningsStatus: "+bStatus+"<br/>BokningsTyp: "+bKind+"<br/>Bokningsbeskrivning: "+bDescription+"<br/><br/></html>");
                sendInvoice[sendInvoiceIncrement] = new JButton("Fakturera");
                sendInvoice[sendInvoiceIncrement].setBackground(Color.ORANGE);
                adminInvoice_Panel.add(printbooking);

                sendInvoice[sendInvoiceIncrement].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                         amount = JOptionPane.showInputDialog(null,"Ange belopp", 1000);
                        int x = sendInvoiceID_Admin;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement a = con.createStatement()){
                            if (amount == null || (amount != null && ("".equals(amount)))){
                                System.out.println("Canceled");
                            }
                            else {
                                for (int z = 0; z < sendInvoice.length; z++) {
                                    if (e.getSource() == sendInvoice[z]) {
                                        x = sendInvoice_button.get(z);

                                        a.execute("UPDATE BookingTable SET bookingStatus = 'Fakturerad' WHERE bookingID = " + x);
                                        String result = bKind.trim() + " " + amount + "kr";

                                        a.execute("UPDATE BookingTable SET bookingKind = '" + result + "' WHERE bookingID = " + x);
                                        adminInvoice_Panel.remove(sendInvoice[z]);

                                    }
                                }
                            }

                        } catch (SQLException d) {
                            System.out.println("Kunde inte connecta");
                        }
                        adminInvoice_Panel.revalidate();
                        adminInvoice_Panel.repaint();
                    }
                });
                sendInvoice_button.add(sendInvoiceID_Admin);
                adminInvoice_Panel.add(sendInvoice[sendInvoiceIncrement]);
                sendInvoiceIncrement++;
            }

            JLabel mybooking_label = new JLabel("Fakturera Kund");
            mybooking_label.setBounds(70,13,250,30);
            mybooking_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);

            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    adminInvoiceTop_TopPanel.setVisible(false);
                    adminInvoice_Panel.setVisible(false);
                    RegisterAdmin.successful_Admin_loginPage.setVisible(true);
                }
            });
            adminInvoiceTop_TopPanel.add(back_label);
            adminInvoiceTop_TopPanel.add(mybooking_label);

        } catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }

    // Panel och metod för en kund att betala en faktura
    static JPanel payInvoiceTOP_TOPPanel = new JPanel();
    static JPanel payInvoice_Panel = new JPanel();
    ArrayList<Integer> payInvoice_button = new ArrayList<>();
    int payInvoiceIncrement = 0;
    int payInvoiceID_User = 1;
    public void userPayInvoice(){
        Main.f.setLayout(new BorderLayout());
        payInvoice_Panel.removeAll();
        payInvoiceTOP_TOPPanel.removeAll();
        payInvoice_Panel.setLayout(new BoxLayout(payInvoice_Panel, BoxLayout.Y_AXIS));
        Register.successful_customer_loginPage.setVisible(false);
        JScrollPane scrollPane = new JScrollPane(payInvoice_Panel,   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(0,150,450,230);
        payInvoiceTOP_TOPPanel.setLayout(null);
        payInvoiceTOP_TOPPanel.setBounds(0,0,500,450);
        payInvoiceTOP_TOPPanel.setVisible(true);
        payInvoiceTOP_TOPPanel.add(scrollPane);
        scrollPane.setVisible(true);
        payInvoice_button.clear();
        payInvoiceIncrement = 0;
        try (Connection con = DriverManager.getConnection(url, username, password);
             Statement start = con.createStatement()) {

            JLabel cardName_label = new JLabel("Namn på betalningskortet");
            cardName_label.setBounds(230,10,150,20);
            JTextField cardName_textField = new JTextField();
            cardName_textField.setBounds(230,30,150,20);

            JLabel cardNumber_label = new JLabel("Kortnummer");
            cardNumber_label.setBounds(230,50,95,20);
            JTextField cardNumber_textField = new JTextField();
            cardNumber_textField.setBounds(230,70,150,20);

            JLabel cardDate_label = new JLabel("MM/YY");
            cardDate_label.setBounds(230,90,95,20);
            JTextField cardDate_textField = new JTextField();
            cardDate_textField.setBounds(230,110,95,20);

            JLabel cardCVC_label = new JLabel("CVC");
            cardCVC_label.setBounds(330,90,80,20);
            JTextField cardCVC_textField = new JTextField();
            cardCVC_textField.setBounds(330,110,80,20);

            payInvoiceTOP_TOPPanel.add(cardName_label);
            payInvoiceTOP_TOPPanel.add(cardName_textField);
            payInvoiceTOP_TOPPanel.add(cardNumber_label);
            payInvoiceTOP_TOPPanel.add(cardNumber_textField);
            payInvoiceTOP_TOPPanel.add(cardDate_label);
            payInvoiceTOP_TOPPanel.add(cardDate_textField);
            payInvoiceTOP_TOPPanel.add(cardCVC_label);
            payInvoiceTOP_TOPPanel.add(cardCVC_textField);

            ResultSet rs = start.executeQuery("SELECT * FROM BookingTable WHERE bookingStatus = 'Fakturerad' AND userID = "+CreateBooking.getId());
            while (rs.next()) {
                JButton[] payInvoice = new JButton[payInvoiceIncrement+1];
                isThereInvoice = true;
                payInvoiceID_User = rs.getInt("bookingID");
                String uID = rs.getString("userID");
                String cID = rs.getString("cleanerID");
                String bDate = rs.getString("bookingDate");
                String bTime = rs.getString("bookingTime");
                String bStatus = rs.getString("bookingStatus");
                String bKind = rs.getString("bookingKind");
                String bDescription = rs.getString("bookingDescription");
                JLabel printbooking = new JLabel("<html>............................................................................................................................................................<br/>BokningsID:" +payInvoiceID_User+"<br/>KundID: "+uID+"<br/>StädareID: "+cID+"<br/>BokningsDatum: "+bDate+"<br/>BokningsTid: "+bTime+"<br/>BokningsStatus: "+bStatus+"<br/>BokningsTyp: "+bKind+"<br/>Bokningsbeskrivning: "+bDescription+"<br/><br/></html>");
                payInvoice[payInvoiceIncrement] = new JButton("Betala");
                payInvoice[payInvoiceIncrement].setBackground(Color.GREEN);
                payInvoice_Panel.add(printbooking);

                payInvoice[payInvoiceIncrement].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int x = payInvoiceID_User;
                        try (Connection con = DriverManager.getConnection(url, username, password);
                             Statement a = con.createStatement()){
                            for (int z = 0; z<payInvoice.length; z++){
                                if (e.getSource() == payInvoice[z]){
                                    if (cardName_textField.getText().length()<3) {
                                        JOptionPane.showMessageDialog(null, "Namn på kortet\nminst 3 bokstäver");
                                    }else if(cardNumber_textField.getText().length()<8){
                                        JOptionPane.showMessageDialog(null, "Kortnummer minst\n8 siffror");
                                    }else if(cardDate_textField.getText().length() !=5){
                                        JOptionPane.showMessageDialog(null, "Kortdatum ska vara\nMM/YY format");
                                    }else if(cardCVC_textField.getText().length() !=3){
                                        JOptionPane.showMessageDialog(null, "CVC ska vara\n 3-4 siffror");
                                    }else {
                                        x = payInvoice_button.get(z);
                                        JOptionPane.showMessageDialog(null, "Betalningen för bokning\n"+x+" är betald");
                                        a.execute("UPDATE BookingTable SET bookingStatus = 'Betald' WHERE bookingID = " + x);
                                        payInvoice_Panel.remove(payInvoice[z]);
                                    }
                                }
                            }
                        } catch (SQLException d) {
                            System.out.println("Kunde inte connecta");
                        }
                        payInvoice_Panel.revalidate();
                        payInvoice_Panel.repaint();
                    }
                });
                payInvoice_button.add(payInvoiceID_User);
                payInvoice_Panel.add(payInvoice[payInvoiceIncrement]);
                payInvoiceIncrement++;
            }
            JLabel info_label = new JLabel("<html>Mata in kortinfo på högra sidan och därefter tryck på Betala knappen (Grön färg) för bokningen som ska betalas</html>");
            info_label.setBounds(5,50,190,100);

            JLabel Paybooking_label = new JLabel("Betala");
            Paybooking_label.setBounds(70,13,150,30);
            Paybooking_label.setFont(new Font("Arial",Font.PLAIN,25));
            // Salehs
            ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
            // Axels
            //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
            Image img = icon.getImage();
            Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
            icon = new ImageIcon(newimg);
            JLabel back_label = new JLabel(icon);
            back_label.setBounds(0,10,50,40);
            back_label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    scrollPane.setVisible(false);
                    payInvoiceTOP_TOPPanel.setVisible(false);
                    Register.successful_customer_loginPage.setVisible(true);
                }
            });
            payInvoiceTOP_TOPPanel.add(back_label);
            payInvoiceTOP_TOPPanel.add(Paybooking_label);
            payInvoiceTOP_TOPPanel.add(info_label);
        }catch (SQLException e){
            System.out.println("Kunde inte ansluta till servern");
        }
    }
}