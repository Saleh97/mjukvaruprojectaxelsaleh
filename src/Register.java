import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Register {

    Scanner scanner = new Scanner(System.in);
    Connect connect = new Connect();
    CreateBooking booking = new CreateBooking();

    public User createUser(String username, String password, String adress, String userKind){
        User user = new User(username, password, adress, userKind);
        Connect connect = new Connect();
        connect.insertUser(username, password, adress,userKind);
        return user;
    }
    static JPanel panel3 = new JPanel();
    boolean userKindCheck = true;
    String userKind;
    // Panel och metod för att registrera nya kunder
    // tar emot vem det är som registrerar ett kundkonto, admin eller kund
    public void addToRegisterList(String kind) {
        panel3.setLayout(null);
        userKindCheck = true;

        // Salehs
        ImageIcon loginImage = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\newCustomer.png");
        // Axel
        //ImageIcon loginImage = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\newCustomer.png");

        Image login_Image = loginImage.getImage();
        Image newLoginImage = login_Image.getScaledInstance(250,300,java.awt.Image.SCALE_SMOOTH);
        loginImage = new ImageIcon(newLoginImage);
        JLabel image_label = new JLabel(loginImage);
        image_label.setBounds(240,0,250 ,340);

        JLabel kundPortal_label = new JLabel("Kundportal");
        kundPortal_label.setBounds(170,0,100,35);
        kundPortal_label.setFont(new Font("Arial",Font.PLAIN,20));

        JLabel loginTitle_label = new JLabel("Registrera");
        loginTitle_label.setBounds(10,45,150,50);
        loginTitle_label.setFont(new Font("Arial",Font.PLAIN,30));

        JLabel chooseUserName = new JLabel("Välj namn");
        chooseUserName.setBounds(10,90,95,30);
        JTextField username = new JTextField();
        username.setBounds(10,120,150,30);
        username.setPreferredSize(new Dimension(150,30));

        JLabel chooseUserPassword = new JLabel("Välj lösenord");
        chooseUserPassword.setBounds(10,150,95,30);
        JPasswordField password = new JPasswordField();
        password.setBounds(10,180,150,30);
        password.setPreferredSize(new Dimension(150,30));

        JLabel chooseUserAdress = new JLabel("Välj adress");
        chooseUserAdress.setBounds(10,210,95,30);
        JTextField adress = new JTextField();
        adress.setBounds(10,240,150,30);
        adress.setPreferredSize(new Dimension(150,30));

        JLabel userKindlabel = new JLabel("Välj typ av kund");
        userKindlabel.setBounds(10,270,95,30);
        JRadioButton r1=new JRadioButton("A) Privat");
        r1.setBounds(10,300,400,30);
        JRadioButton r2=new JRadioButton("B) Företag");
        r2.setBounds(10,330,450,30);
        ButtonGroup bg=new ButtonGroup();
        bg.add(r1);bg.add(r2);

        JButton register = new JButton("Registrera");
        register.setBounds(10,370,150,30);
        register.setBackground(Color.BLUE);
        register.setForeground(Color.white);

        // Salehs
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axels
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                panel3.setVisible(false);
                if (kind =="kund") {
                    userlogInPanel.setVisible(true);
                }
                else if(kind == "admin"){
                    RegisterAdmin.administrateUserMenu_Panel.setVisible(true);
                }
                else {
                    Main.panel2.setVisible(true);
                }
                username.setText("");
                adress.setText("");
                password.setText("");
                bg.clearSelection();
                panel3.removeAll();
            }
        });

            panel3.add(chooseUserName);
            panel3.add(username);
            panel3.add(chooseUserPassword);
            panel3.add(password);
            panel3.add(chooseUserAdress);
            panel3.add(adress);
            panel3.add(userKindlabel);
            panel3.add(r1);
            panel3.add(r2);
            panel3.add(register);
            panel3.add(image_label);
            panel3.add(kundPortal_label);
            panel3.add(loginTitle_label);
            panel3.add(back_label);
            register.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (username.getText().length() < 4 || password.getPassword().length<6){
                        JOptionPane.showMessageDialog(null, "namn minst 4 tecken \nLösenordet minst 6 tecken");
                    }else{
                        if (r1.isSelected()){
                            userKind ="Privat";
                            createUser(username.getText(), password.getText(), adress.getText(), userKind);
                            JOptionPane.showMessageDialog(null,"Registreringen Lyckades");
                            panel3.setVisible(false);
                            if (kind =="kund") {
                                Main.panel2.setVisible(true);
                            }
                            else if(kind == "admin"){
                                RegisterAdmin.administrateUserMenu_Panel.setVisible(true);
                            }
                            username.setText("");
                            password.setText("");
                            adress.setText("");
                            bg.clearSelection();
                            panel3.removeAll();

                        }else if (r2.isSelected()){
                            userKind = "Företag";
                            createUser(username.getText(), password.getText(), adress.getText(), userKind);
                            JOptionPane.showMessageDialog(null,"Registreringen Lyckades");
                            panel3.setVisible(false);
                            if (kind =="kund") {
                                Main.panel2.setVisible(true);
                            }
                            else if(kind == "admin"){
                                RegisterAdmin.administrateUserMenu_Panel.setVisible(true);
                            }
                            username.setText("");
                            password.setText("");
                            adress.setText("");
                            bg.clearSelection();
                            panel3.removeAll();
                        } else{
                            JOptionPane.showMessageDialog(null, "Välj mellan privat och företag");
                        }
                    }
                }
            });
    }

    // Panel och metod för kundlogin
    static JPanel userlogInPanel = new JPanel();
    JTextField checkUsername = new JTextField();
    public void logInInput(){
        userlogInPanel.setLayout(null);
        // Salehs
        ImageIcon loginImage = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\loginCustomer.png");
        // Axels
        //ImageIcon loginImage = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\loginCustomer.png");
        Image login_Image = loginImage.getImage();
        Image newLoginImage = login_Image.getScaledInstance(200,200,java.awt.Image.SCALE_SMOOTH);
        loginImage = new ImageIcon(newLoginImage);
        JLabel image_label = new JLabel(loginImage);
        image_label.setBounds(240,50,200,200);

        JLabel kundPortal_label = new JLabel("Kundportal");
        kundPortal_label.setBounds(170,0,100,35);
        kundPortal_label.setFont(new Font("Arial",Font.PLAIN,20));

        JLabel loginTitle_label = new JLabel("Logga in");
        loginTitle_label.setBounds(10,50,150,50);
        loginTitle_label.setFont(new Font("Arial",Font.PLAIN,30));

        JLabel chooseUserName = new JLabel("Användarnamn");
        chooseUserName.setBounds(10,100,95,30);

        checkUsername.setBounds(10,130,150,30);
        checkUsername.setPreferredSize(new Dimension(150,30));
        JLabel chooseUserPassword = new JLabel("Lösenord");
        chooseUserPassword.setBounds(10,160,95,30);
        JPasswordField password = new JPasswordField();
        password.setBounds(10,190,150,30);
        password.setPreferredSize(new Dimension(150,30));

        // Logga in knapp
        JButton logInButton = new JButton("Logga in");
        logInButton.setBounds(10,230,150,30);
        logInButton.setBackground(Color.GREEN);

        // Salehs
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axels
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                userlogInPanel.setVisible(false);
                Main.panel1.setVisible(true);
                checkUsername.setText("");
                password.setText("");
            }
        });
        JLabel register_label = new JLabel("<html>Inget konto? Registrera</html>");
        register_label.setBounds(10,260,230,30);
        // Metod för vad som händer när man klickar på en JLabel
        register_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addToRegisterList("kund");
                userlogInPanel.setVisible(false);
                panel3.setBounds(0,5,500 ,450);
                panel3.setVisible(true);
            }
        });

        userlogInPanel.add(chooseUserName);
        userlogInPanel.add(checkUsername);
        userlogInPanel.add(chooseUserPassword);
        userlogInPanel.add(password);
        userlogInPanel.add(logInButton);
        userlogInPanel.add(register_label);
        userlogInPanel.add(loginTitle_label);
        userlogInPanel.add(back_label);
        userlogInPanel.add(image_label);
        userlogInPanel.add(kundPortal_label);
        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.checkLogInUser(checkUsername.getText(),password.getText());
                checkUsername.setText("");
                password.setText("");
            }
        });
    }

    // Panel och metod för kund startsida/lyckad login
    static JPanel successful_customer_loginPage = new JPanel(){
        // Sätter bakgrundsbild för panelen
        @Override
        protected void paintComponent (Graphics g){
            Image img = null;
            try {
                //Salehs
                img = ImageIO.read(new File("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\customerSuccess.jpg"));
                //Axels
                //img = ImageIO.read(new File("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\customerSuccess.jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(img,0,0,null);
        }
    };
    public void successful_login(String username, String password){
            successful_customer_loginPage.setLayout(null);
            successful_customer_loginPage.setBounds(0,0,500,450);
            userlogInPanel.setVisible(false);

            JLabel welcomeLabel = new JLabel("Välkommen, " + username);
            welcomeLabel.setBounds(105,0,500,100);
            welcomeLabel.setFont(new Font("Arial",Font.PLAIN,30));

            // Salehs
            ImageIcon bookIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\bookUser.png");
            // Axels
            //ImageIcon bookIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\bookUser.png");
            Image myBookImg = bookIcon.getImage();
            Image newMyBookimg = myBookImg.getScaledInstance(40,40,java.awt.Image.SCALE_SMOOTH);
            bookIcon = new ImageIcon(newMyBookimg);

            JButton bookButton = new JButton("<html>Boka<br/>Städning</html>",bookIcon);
            bookButton.setBounds(50,100,140,45);
            bookButton.setVerticalTextPosition(SwingConstants.CENTER);
            bookButton.setHorizontalAlignment(SwingConstants.LEFT);

            //MyPage button
            // Salehs
            ImageIcon myPageIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\mypageUser.png");
            // Axels
            //ImageIcon myPageIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\mypageUser.png");
            Image myPageImg = myPageIcon.getImage();
            Image newMyPageimg = myPageImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
            myPageIcon = new ImageIcon(newMyPageimg);

            JButton myPageButton = new JButton("Mina Sidor",myPageIcon);
            myPageButton.setBounds(250,100,140,45);
            myPageButton.setVerticalTextPosition(SwingConstants.CENTER);
            myPageButton.setHorizontalAlignment(SwingConstants.LEFT);

            //My Booking button
            // Salehs
            ImageIcon myBookingIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\mybookingUser2.png");
            // Axels
            //ImageIcon myBookingIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\mybookingUser2.png");

            Image myBookingImg = myBookingIcon.getImage();
            Image newMyBookingimg = myBookingImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
            myBookingIcon = new ImageIcon(newMyBookingimg);

            JButton mybookingButton = new JButton("<html>Bokade<br/>Städningar</html>",myBookingIcon);
            mybookingButton.setBounds(50,180,140,45);
            mybookingButton.setVerticalTextPosition(SwingConstants.CENTER);
            mybookingButton.setHorizontalAlignment(SwingConstants.LEFT);

            //Done booking button
            // Salehs
            ImageIcon doneIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\utfördaUser2.png");
            // Axels
            //ImageIcon doneIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\utfördaUser2.png");
            Image doneImg = doneIcon.getImage();
            Image newDoneimg = doneImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
            doneIcon = new ImageIcon(newDoneimg);

            JButton doneBookingButton = new JButton("<html>Utförda<br/>Städningar</html>",doneIcon);
            doneBookingButton.setBounds(250,180,140,45);
            doneBookingButton.setVerticalTextPosition(SwingConstants.CENTER);
            doneBookingButton.setHorizontalAlignment(SwingConstants.LEFT);

            // Remove button
            // Salehs
            ImageIcon removeIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\removeUser.png");
            // Axels
            //ImageIcon removeIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\removeUser.png");

            Image removeImg = removeIcon.getImage();
            Image newRemoveimg = removeImg.getScaledInstance(40,30,java.awt.Image.SCALE_SMOOTH);
            removeIcon = new ImageIcon(newRemoveimg);

            JButton removebookingButton = new JButton("<html>Radera<br/>Städning</html>",removeIcon);
            removebookingButton.setBounds(50,260,140,45);
            removebookingButton.setVerticalTextPosition(SwingConstants.CENTER);
            removebookingButton.setHorizontalAlignment(SwingConstants.LEFT);

            //Pay button
            // Salehs
            ImageIcon payIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\betalaUser.png");
            // Axels
            //ImageIcon payIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\betalaUser.png");

            Image payImg = payIcon.getImage();
            Image newPayimg = payImg.getScaledInstance(40,30,java.awt.Image.SCALE_SMOOTH);
            payIcon = new ImageIcon(newPayimg);

            JButton payButton = new JButton("Betala",payIcon);
            payButton.setBounds(250,260,140,45);
            payButton.setVerticalTextPosition(SwingConstants.CENTER);
            payButton.setHorizontalAlignment(SwingConstants.LEFT);

            // Logout button
            // Salehs
            ImageIcon logoutIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\logout3.png");
            // Axels
            //ImageIcon logoutIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\logout3.png");
            Image logoutImg = logoutIcon.getImage();
            Image newlogoutimg = logoutImg.getScaledInstance(40,30,java.awt.Image.SCALE_SMOOTH);
            logoutIcon = new ImageIcon(newlogoutimg);

            JButton logout_button = new JButton("Logga Ut",logoutIcon);
            logout_button.setBounds(150,340,140,45);
            logout_button.setVerticalTextPosition(SwingConstants.CENTER);
            logout_button.setHorizontalAlignment(SwingConstants.LEFT);

            successful_customer_loginPage.add(logout_button);
            successful_customer_loginPage.add(bookButton);
            successful_customer_loginPage.add(myPageButton);
            successful_customer_loginPage.add(mybookingButton);
            successful_customer_loginPage.add(doneBookingButton);
            successful_customer_loginPage.add(removebookingButton);
            successful_customer_loginPage.add(payButton);
            successful_customer_loginPage.add(welcomeLabel);

            logout_button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    successful_customer_loginPage.setVisible(false);
                    Main.panel1.setVisible(true);
                    successful_customer_loginPage.removeAll();
                }
            });
            bookButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    booking.addBooking();
                }
            });
            myPageButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //myPage(username,password);
                    connect.myPageInfoUser(username,password,"user");
                }
            });
            mybookingButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    connect.showAllUserBookings();
                }
            });
            doneBookingButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    connect.showAllDoneUserBookings();
                }
            });
            removebookingButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    connect.deleteBookingUser();
                }
            });
            payButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    connect.userPayInvoice();
                }
            });
    }
}
