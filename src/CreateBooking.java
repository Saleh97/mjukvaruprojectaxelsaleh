import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateBooking {
    Connect connect = new Connect();
    private static int id = 1;

    public static int getId() {
        return id;
    }
    public static void setId(int id) {
        CreateBooking.id = id;
    }

   // ArrayList<Booking> bookingList = new ArrayList<>();

    // Skapar en bokning och skickar vidare info till checkBooking
    public Booking createBoking(int userID, int cleanerID, String bDate, String btime, String adress, String bDescription, String bStatus, String bookingKind, int cost){
        Booking booking = new Booking(userID,cleanerID,bDate,btime,adress,bDescription,bStatus,bookingKind);
        connect.checkBooking(userID,cleanerID,bDate,btime,adress,bDescription,bStatus,bookingKind);
        return booking;
    }

    // Panel och metod för sidan man kan skapa en bokning på
    String bookingKindd = "";
    static JPanel addBoking_Panel = new JPanel();
    public void addBooking(){
        addBoking_Panel.setLayout(null);
        addBoking_Panel.removeAll();
        addBoking_Panel.setBounds(0,10,500,450);
        Register.successful_customer_loginPage.setVisible(false);
        addBoking_Panel.setVisible(true);
        int cost = 0;

        // Salehs
        ImageIcon bookImage = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\book.png");
        // Axels
        //ImageIcon loginImage = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\book.png");
        Image login_Image = bookImage.getImage();
        Image newLoginImage = login_Image.getScaledInstance(280,280,java.awt.Image.SCALE_SMOOTH);
        bookImage = new ImageIcon(newLoginImage);
        JLabel image_label = new JLabel(bookImage);
        image_label.setBounds(200,40,280,280);

        JLabel adress_label = new JLabel("Adress");
        adress_label.setBounds(10,30,95,25);
        JTextField adress_field = new JTextField();
        adress_field.setBounds(10,55,150,30);

        JLabel date_label = new JLabel("Datum: DD/MM/YY");
        date_label.setBounds(10,85,130,25);
        JTextField dateInput_label = new JTextField();
        dateInput_label.setBounds(10,110,150,30);

        String regex = "^[0-3]?[0-9]/[0-3]?[0-9]/(?:[0-9]{2})?[0-9]{2}$";
        Pattern pattern = Pattern.compile(regex);

        JLabel timee_label = new JLabel("Tid: HH:MM");
        timee_label.setBounds(10,140,95,25);
        JTextField timeeInput_label = new JTextField();
        timeeInput_label.setBounds(10,165,150,30);

        String regexTime = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
        Pattern patternTime = Pattern.compile(regexTime);

        JLabel description_label = new JLabel("Beskrivning");
        description_label.setBounds(10,195,95,25);
        JTextField descriptionInput_label = new JTextField();
        descriptionInput_label.setBounds(10,220,150,30);

        JLabel bookingKind_label = new JLabel("Välj typ av städning");
        bookingKind_label.setBounds(10,260,140,20);
        JRadioButton k1=new JRadioButton("Basic Städning");
        k1.setBounds(10,280,140,20);
        JRadioButton k2=new JRadioButton("Topp Städning");
        k2.setBounds(10,300,140,20);
        JRadioButton k3=new JRadioButton("Diamant Städning");
        k3.setBounds(10,320,140,20);
        JRadioButton k4=new JRadioButton("Fönstertvätt");
        k4.setBounds(10,340,140,20);
        ButtonGroup bg=new ButtonGroup();
        bg.add(k1);bg.add(k2);bg.add(k3);bg.add(k4);
        JButton addBoking_Button = new JButton("Skapa bokning");
        addBoking_Button.setBounds(10,370,150,30);
        addBoking_Button.setBackground(Color.GREEN);
        // Salehs
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axels
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(40,30,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,40,30);

        addBoking_Panel.add(adress_label);
        addBoking_Panel.add(adress_field);
        addBoking_Panel.add(date_label);
        addBoking_Panel.add(dateInput_label);
        addBoking_Panel.add(timee_label);
        addBoking_Panel.add(timeeInput_label);
        addBoking_Panel.add(description_label);
        addBoking_Panel.add(descriptionInput_label);
        addBoking_Panel.add(bookingKind_label);
        addBoking_Panel.add(k1);
        addBoking_Panel.add(k2);
        addBoking_Panel.add(k3);
        addBoking_Panel.add(k4);
        addBoking_Panel.add(addBoking_Button);
        addBoking_Panel.add(back_label);
        addBoking_Panel.add(image_label);

        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addBoking_Panel.setVisible(false);
                Register.successful_customer_loginPage.setVisible(true);
                bg.clearSelection();
                adress_field.setText("");
                timeeInput_label.setText("");
                dateInput_label.setText("");
                descriptionInput_label.setText("");
            }
        });

        addBoking_Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (k1.isSelected()){
                    bookingKindd = "Basic Städning";
                }else if(k2.isSelected()){
                    bookingKindd="Topp Städning";
                }else if(k3.isSelected()){
                    bookingKindd="Diamant Städning";
                }else if(k4.isSelected()){
                    bookingKindd = "Fönstertvätt";
                }else {
                    bookingKindd ="";
                }
                Matcher matcher = pattern.matcher(dateInput_label.getText());
                Matcher matcherTime = patternTime.matcher(timeeInput_label.getText());
                if (matcher.matches() == false) {
                    JOptionPane.showMessageDialog(null, "Datumet måste vara i\n detta formatet DD/MM/YY");
                }else if(matcherTime.matches() == false){
                    JOptionPane.showMessageDialog(null, "Tiden måste vara i\n detta formatet HH:MM");
                }else if(adress_field.getText().length() < 3){
                    JOptionPane.showMessageDialog(null, "Fyll i en giltig adress");
                }else if(descriptionInput_label.getText().length() < 3){
                    JOptionPane.showMessageDialog(null, "Fyll i beskrivningsfältet\nSkriv ditt för & efternamn");
                } else if (k1.isSelected() == false && k2.isSelected() == false && k3.isSelected() == false && k4.isSelected() == false){
                    JOptionPane.showMessageDialog(null, "Välj typ av städning");
                }else {
                    createBoking(getId(), 1, dateInput_label.getText(), timeeInput_label.getText(), adress_field.getText(), descriptionInput_label.getText(), "Obekräftad", bookingKindd, cost);
                    addBoking_Panel.setVisible(false);
                    Register.successful_customer_loginPage.setVisible(true);
                    bg.clearSelection();
                    adress_field.setText("");
                    timeeInput_label.setText("");
                    dateInput_label.setText("");
                    descriptionInput_label.setText("");
                }
            }
        });
    }
}
