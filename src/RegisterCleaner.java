import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class RegisterCleaner {
    Scanner scanner = new Scanner(System.in);
    Connect connect = new Connect();
    int x = -1;
    public Cleaner createCleaner(String username, String password, String adress){
        Cleaner cleaner = new Cleaner(username, password, adress);
        x++;
        cleaner.setId(x);
        connect.insertCleaner(username,password,adress);
        return cleaner;
    }

    // Panel och metod för registrera städare sida
    static JPanel cleanerRegister = new JPanel();
    public void addToRegisterCleanerList(String kind){
        cleanerRegister.setLayout(null);
        ImageIcon loginImage = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\cleanerRegister1.png");
        Image login_Image = loginImage.getImage();
        Image newLoginImage = login_Image.getScaledInstance(250,300,java.awt.Image.SCALE_SMOOTH);
        loginImage = new ImageIcon(newLoginImage);
        JLabel image_label = new JLabel(loginImage);
        image_label.setBounds(240,0,250 ,340);

        JLabel loginTitle_label = new JLabel("Registrera Städare");
        loginTitle_label.setBounds(10,45,180,50);
        loginTitle_label.setFont(new Font("Arial",Font.PLAIN,20));

        JLabel chooseUserName = new JLabel("Välj namn");
        chooseUserName.setBounds(10,90,95,30);
        JTextField username = new JTextField();
        username.setBounds(10,120,150,30);

        JLabel chooseUserPassword = new JLabel("Välj lösenord");
        chooseUserPassword.setBounds(10,150,95,30);
        JPasswordField password = new JPasswordField();
        password.setBounds(10,180,150,30);

        JLabel chooseUserAdress = new JLabel("Välj adress");
        chooseUserAdress.setBounds(10,210,95,30);
        JTextField adress = new JTextField();
        adress.setBounds(10,240,150,30);

        JButton register = new JButton("Registrera");
        register.setBounds(10,280,150,30);
        register.setBackground(Color.BLUE);
        register.setForeground(Color.white);

        // Salehs
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axels
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                cleanerRegister.setVisible(false);
                if (kind == "admin"){
                    RegisterAdmin.administrateCleanerMenu_Panel.setVisible(true);
                } else if (kind == "cleaner") {
                    Main.cleanerStartPanel.setVisible(true);
                }
                username.setText("");
                adress.setText("");
                password.setText("");
                cleanerRegister.removeAll();
            }
        });

        cleanerRegister.add(chooseUserName);
        cleanerRegister.add(username);
        cleanerRegister.add(chooseUserPassword);
        cleanerRegister.add(password);
        cleanerRegister.add(chooseUserAdress);
        cleanerRegister.add(adress);
        cleanerRegister.add(register);
        cleanerRegister.add(back_label);
        cleanerRegister.add(image_label);
        cleanerRegister.add(loginTitle_label);

        register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (username.getText().length() < 4 || password.getPassword().length<6){
                    JOptionPane.showMessageDialog(null, "namn minst 4 tecken \nLösenordet minst 6 tecken");
                } else {
                    createCleaner(username.getText(), password.getText(), adress.getText());
                    JOptionPane.showMessageDialog(null,"Registreringen Lyckades");
                    cleanerRegister.setVisible(false);
                    if (kind == "admin"){
                        RegisterAdmin.administrateCleanerMenu_Panel.setVisible(true);
                    } else if (kind == "cleaner"){
                        Main.cleanerStartPanel.setVisible(true);
                    }
                    username.setText("");
                    password.setText("");
                    adress.setText("");
                    cleanerRegister.removeAll();
                }
            }
        });
    }

    // Logga in sidan och panel för städare
    static JPanel cleanerLoginPanel = new JPanel();
    public void logInInput(){
        cleanerLoginPanel.setLayout(null);
        // Salehs
        ImageIcon loginImage = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\Cleanerlogin.png");
        // Axels
        //ImageIcon loginImage = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\Cleanerlogin.png");

        Image login_Image = loginImage.getImage();
        Image newLoginImage = login_Image.getScaledInstance(250,300,java.awt.Image.SCALE_SMOOTH);
        loginImage = new ImageIcon(newLoginImage);
        JLabel image_label = new JLabel(loginImage);
        image_label.setBounds(240,40,250 ,350);

        JLabel cleanerPortal_label = new JLabel("Städareportal");
        cleanerPortal_label.setBounds(170,0,150,35);
        cleanerPortal_label.setFont(new Font("Arial",Font.PLAIN,20));

        JLabel loginTitle_label = new JLabel("Logga in");
        loginTitle_label.setBounds(10,50,150,50);
        loginTitle_label.setFont(new Font("Arial",Font.PLAIN,30));

        JLabel chooseUserName = new JLabel("Användarnamn");
        chooseUserName.setBounds(10,100,95,30);
        JTextField username = new JTextField();
        username.setBounds(10,130,150,30);
        username.setPreferredSize(new Dimension(150,30));
        JLabel chooseUserPassword = new JLabel("Lösenord");
        chooseUserPassword.setBounds(10,160,95,30);
        JPasswordField password = new JPasswordField();
        password.setBounds(10,190,150,30);
        password.setPreferredSize(new Dimension(150,30));
        JButton logInButton = new JButton("Logga in");
        logInButton.setBounds(10,230,150,30);
        logInButton.setBackground(Color.GREEN);
        // Salehs
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axels
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                cleanerLoginPanel.setVisible(false);
                Main.panel1.setVisible(true);
                username.setText("");
                password.setText("");
            }
        });
        cleanerLoginPanel.add(chooseUserName);
        cleanerLoginPanel.add(username);
        cleanerLoginPanel.add(chooseUserPassword);
        cleanerLoginPanel.add(password);
        cleanerLoginPanel.add(logInButton);
        cleanerLoginPanel.add(loginTitle_label);
        cleanerLoginPanel.add(back_label);
        cleanerLoginPanel.add(image_label);
        cleanerLoginPanel.add(cleanerPortal_label);
        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.checkLogInCleaner(username.getText(),password.getText());
                username.setText("");
                password.setText("");
            }
        });
    }

    static JPanel successful_cleaner_loginPage = new JPanel(){
        @Override
        protected void paintComponent (Graphics g){
            Image img = null;
            try {
                //Salehs
                img = ImageIO.read(new File("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\cleanerSuccess.jpg"));
                //Axels
                //img = ImageIO.read(new File("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\cleanerSuccess.jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(img,0,0,null);
        }
    };
    public void successful_login_Cleaner(String username, String password){
        successful_cleaner_loginPage.setLayout(null);
        successful_cleaner_loginPage.setBounds(0,0,500,450);
        cleanerLoginPanel.setVisible(false);

        JLabel welcomeLabel = new JLabel("Välkommen, " + username);
        welcomeLabel.setBounds(105,0,500,100);
        welcomeLabel.setFont(new Font("Arial",Font.PLAIN,30));


        // Tilldelade städningar button
        // Salehs
        ImageIcon bookIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\tilldeladeStädningar.png");
        // Axels
        //ImageIcon bookIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\tilldeladeStädningar.png");
        Image myBookImg = bookIcon.getImage();
        Image newMyBookimg = myBookImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        bookIcon = new ImageIcon(newMyBookimg);

        JButton myBookingsbtn = new JButton("<html>Tilldelade<br/>Städningar</html>",bookIcon);
        myBookingsbtn.setBounds(50,100,140,45);
        myBookingsbtn.setVerticalTextPosition(SwingConstants.CENTER);
        myBookingsbtn.setHorizontalAlignment(SwingConstants.LEFT);

        // MyPage button
        // Salehs
        ImageIcon myPageIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\mypageUser.png");
        // Axels
        //ImageIcon myPageIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\mypageUser.png");
        Image myPageImg = myPageIcon.getImage();
        Image newMyPageimg = myPageImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        myPageIcon = new ImageIcon(newMyPageimg);

        JButton myPageButton = new JButton("Mina Sidor",myPageIcon);
        myPageButton.setBounds(250,100,140,45);
        myPageButton.setVerticalTextPosition(SwingConstants.CENTER);
        myPageButton.setHorizontalAlignment(SwingConstants.LEFT);

        // Bekräftade städningar button
        // Salehs
        ImageIcon myBookingIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\mybookingUser2.png");
        // Axels
        //ImageIcon myBookingIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\mybookingUser2.png");

        Image myBookingImg = myBookingIcon.getImage();
        Image newMyBookingimg = myBookingImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        myBookingIcon = new ImageIcon(newMyBookingimg);

        JButton myConfirmedBookingsbtn = new JButton("<html>Bekräftade<br/>Städningar</html>",myBookingIcon);
        myConfirmedBookingsbtn.setBounds(50,180,140,45);
        myConfirmedBookingsbtn.setVerticalTextPosition(SwingConstants.CENTER);
        myConfirmedBookingsbtn.setHorizontalAlignment(SwingConstants.LEFT);

        // Genomförda städningar button
        // Salehs
        ImageIcon doneIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\utfördaUser2.png");
        // Axels
        //ImageIcon doneIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\utfördaUser2.png");
        Image doneImg = doneIcon.getImage();
        Image newDoneimg = doneImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        doneIcon = new ImageIcon(newDoneimg);

        JButton myDoneBookingsbtn = new JButton("<html>Genomförda<br/>Städningar</html>",doneIcon);
        myDoneBookingsbtn.setBounds(250,180,140,45);
        myDoneBookingsbtn.setVerticalTextPosition(SwingConstants.CENTER);
        myDoneBookingsbtn.setHorizontalAlignment(SwingConstants.LEFT);


        // Logout button
        // Salehs
        ImageIcon logoutIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\logout3.png");
        // Axels
        //ImageIcon logoutIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\logout3.png");
        Image logoutImg = logoutIcon.getImage();
        Image newlogoutimg = logoutImg.getScaledInstance(40,30,java.awt.Image.SCALE_SMOOTH);
        logoutIcon = new ImageIcon(newlogoutimg);

        JButton logout_button = new JButton("Logga Ut",logoutIcon);
        logout_button.setBounds(150,340,140,45);
        logout_button.setVerticalTextPosition(SwingConstants.CENTER);
        logout_button.setHorizontalAlignment(SwingConstants.LEFT);

        successful_cleaner_loginPage.add(myBookingsbtn);
        successful_cleaner_loginPage.add(myConfirmedBookingsbtn);
        successful_cleaner_loginPage.add(myDoneBookingsbtn);
        successful_cleaner_loginPage.add(myPageButton);
        successful_cleaner_loginPage.add(logout_button);
        successful_cleaner_loginPage.add(welcomeLabel);
        logout_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                successful_cleaner_loginPage.setVisible(false);
                Main.panel1.setVisible(true);
                successful_cleaner_loginPage.removeAll();
            }
        });

        myBookingsbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.cleanerShowAllMyAssignedBooking(username, password);
            }
        });

        myConfirmedBookingsbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.cleanerShowAllConfirmedBooking(username, password);
            }
        });

        myDoneBookingsbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.cleanerShowAllDoneBooking(username, password);
            }
        });

        myPageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.myPageInfoUser(username,password,"cleaner");
            }
        });
    }
}
