import java.util.UUID;

public class Booking {
    private int id;
    private String description;
    private String adress;
    private String date;
    private String time;
    private int user;
    private int cleaner;
    private String status;


    private String bookingKind;

    public Booking(int userID, int cleanerID, String bDate, String btime, String adress, String bDescription, String bStatus, String bookingKind) {
        this.description = bDescription;
        this.adress = adress;
        this.date = bDate;
        this.time = btime;
        this.user = userID;
        this.cleaner = cleanerID;
        this.status = bStatus;
        this.bookingKind = bookingKind;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getCleaner() {
        return cleaner;
    }

    public void setCleaner(int cleaner) {
        this.cleaner = cleaner;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBookingKind() {
        return bookingKind;
    }

    public void setBookingKind(String bookingKind) {
        this.bookingKind = bookingKind;
    }

}
