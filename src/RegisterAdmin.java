import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class RegisterAdmin {
    Scanner scanner = new Scanner(System.in);
    Connect connect = new Connect();

    public Admin createAdmin(String username, String password){
        Admin admin = new Admin(username, password);
        connect.insertAdmin(username,password);
        return admin;
    }

    // Panel och metod för att registrera admin
    // Ta bort?
    static JPanel adminRegister_Panel = new JPanel();
    public void addToRegisterAdminList(){
        JLabel chooseUserName = new JLabel("Välj namn");
        chooseUserName.setBounds(50,50,95,30);
        JTextField username = new JTextField();
        username.setBounds(50,100,150,30);
        username.setPreferredSize(new Dimension(150,30));

        JLabel chooseUserPassword = new JLabel("Välj lösenord");
        chooseUserPassword.setBounds(100,150,95,30);
        JPasswordField password = new JPasswordField();
        password.setBounds(100,200,150,30);
        password.setPreferredSize(new Dimension(150,30));

        JButton register = new JButton("Registrera");
        register.setBounds(50,500,95,30);
        adminRegister_Panel.add(chooseUserName);
        adminRegister_Panel.add(username);
        adminRegister_Panel.add(chooseUserPassword);
        adminRegister_Panel.add(password);
        adminRegister_Panel.add(register);

        register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (username.getText().length() < 4 || password.getPassword().length<6){
                    JOptionPane.showMessageDialog(null, "namn minst 4 tecken \nLösenordet minst 6 tecken");
                } else {
                    createAdmin(username.getText(), password.getText());
                    JOptionPane.showMessageDialog(null,"Registreringen Lyckades");
                    adminRegister_Panel.setVisible(false);
                    Main.adminStartPanel.setVisible(true);
                    username.setText("");
                    password.setText("");
                }
            }
        });

        JButton back_button = new JButton("Tillbaka");
        back_button.setBounds(50,550,95,30);
        adminRegister_Panel.add(back_button);
        back_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adminRegister_Panel.setVisible(false);
                Main.adminStartPanel.setVisible(true);
                username.setText("");
                password.setText("");
            }
        });

    }

    // Panel och metod för admin loginsida
    static JPanel adminLogin_Panel = new JPanel();
    public void logInInput(){
        adminLogin_Panel.setLayout(null);
        // Saleh
        ImageIcon loginImage = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\Adminlogin.png");
        // Axel
        //ImageIcon loginImage = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\Adminlogin.png");
        Image login_Image = loginImage.getImage();
        Image newLoginImage = login_Image.getScaledInstance(200,200,java.awt.Image.SCALE_SMOOTH);
        loginImage = new ImageIcon(newLoginImage);
        JLabel image_label = new JLabel(loginImage);
        image_label.setBounds(240,70,200,200);

        JLabel adminPortal_label = new JLabel("Adminportal");
        adminPortal_label.setBounds(170,0,150,35);
        adminPortal_label.setFont(new Font("Arial",Font.PLAIN,20));

        JLabel loginTitle_label = new JLabel("Logga in");
        loginTitle_label.setBounds(10,50,150,50);
        loginTitle_label.setFont(new Font("Arial",Font.PLAIN,30));

        JLabel chooseUserName = new JLabel("Användarnamn");
        chooseUserName.setBounds(10,100,95,30);
        JTextField username = new JTextField();
        username.setBounds(10,130,150,30);
        username.setPreferredSize(new Dimension(150,30));
        JLabel chooseUserPassword = new JLabel("Lösenord");
        chooseUserPassword.setBounds(10,160,95,30);
        JPasswordField password = new JPasswordField();
        password.setBounds(10,190,150,30);
        password.setPreferredSize(new Dimension(150,30));
        JButton logInButton = new JButton("Logga in");
        logInButton.setBounds(10,230,150,30);
        logInButton.setBackground(Color.GREEN);

        // Saleh
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axel
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                adminLogin_Panel.setVisible(false);
                Main.panel1.setVisible(true);
                username.setText("");
                password.setText("");
            }
        });

        adminLogin_Panel.add(chooseUserName);
        adminLogin_Panel.add(username);
        adminLogin_Panel.add(chooseUserPassword);
        adminLogin_Panel.add(password);
        adminLogin_Panel.add(logInButton);
        adminLogin_Panel.add(loginTitle_label);
        adminLogin_Panel.add(back_label);
        adminLogin_Panel.add(image_label);
        adminLogin_Panel.add(adminPortal_label);

        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.checkLogInAdmin(username.getText(),password.getText());
                username.setText("");
                password.setText("");
            }
        });
    }

    // Panel och metod för admin startsida/lyckad login
    static JPanel successful_Admin_loginPage = new JPanel(){
        @Override
        protected void paintComponent (Graphics g){
            Image img = null;
            try {
                //Salehs
                img = ImageIO.read(new File("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\adminSuccess.jpg"));
                //Axels
                //img = ImageIO.read(new File("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\adminSuccess.jpg"));

            } catch (IOException e) {
                e.printStackTrace();
            }
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(img,0,0,null);
        }
    };
    public void successful_login_Admin(String username, String password){
        successful_Admin_loginPage.setLayout(null);
        successful_Admin_loginPage.setBounds(0,0,500,450);
        adminLogin_Panel.setVisible(false);
        successful_Admin_loginPage.setVisible(true);

        JLabel welcomeLabel = new JLabel("Välkommen, " + username);
        welcomeLabel.setBounds(105,0,500,100);
        welcomeLabel.setFont(new Font("Arial",Font.PLAIN,30));

        // Administrera kund button
        // Salehs
        ImageIcon bookIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\administrateUser.png");
        // Axels
        //ImageIcon bookIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\administrateUser.png");
        Image myBookImg = bookIcon.getImage();
        Image newMyBookimg = myBookImg.getScaledInstance(40,30,java.awt.Image.SCALE_SMOOTH);
        bookIcon = new ImageIcon(newMyBookimg);

        JButton administrate_User_button = new JButton("<html>Administrera<br/>Kund</html>",bookIcon);
        administrate_User_button.setBounds(50,100,140,45);
        administrate_User_button.setVerticalTextPosition(SwingConstants.CENTER);
        administrate_User_button.setHorizontalAlignment(SwingConstants.LEFT);

        // MyPage button
        // Salehs
        ImageIcon myPageIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\mypageUser.png");
        // Axels
        //ImageIcon myPageIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\mypageUser.png");
        Image myPageImg = myPageIcon.getImage();
        Image newMyPageimg = myPageImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        myPageIcon = new ImageIcon(newMyPageimg);

        JButton myPage_Admin_button = new JButton("Mina Sidor",myPageIcon);
        myPage_Admin_button.setBounds(250,100,140,45);
        myPage_Admin_button.setVerticalTextPosition(SwingConstants.CENTER);
        myPage_Admin_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Bekräftade städningar button
        // Salehs
        ImageIcon myBookingIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\AdministrateCleaner2.png");
        // Axels
        //ImageIcon myBookingIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\AdministrateCleaner2.png");
        Image myBookingImg = myBookingIcon.getImage();
        Image newMyBookingimg = myBookingImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        myBookingIcon = new ImageIcon(newMyBookingimg);

        JButton administrate_Cleaner_button = new JButton("<html>Administrera<br/>Städare</html>",myBookingIcon);
        administrate_Cleaner_button.setBounds(50,180,140,45);
        administrate_Cleaner_button.setVerticalTextPosition(SwingConstants.CENTER);
        administrate_Cleaner_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Administrera bokning button
        // Salehs
        ImageIcon doneIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\administrateBooking.png");
        // Axels
        //ImageIcon doneIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\administrateBooking.png");
        Image doneImg = doneIcon.getImage();
        Image newDoneimg = doneImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        doneIcon = new ImageIcon(newDoneimg);

        JButton administrate_Booking_button = new JButton("<html>Administrera<br/>Bokning</html>",doneIcon);
        administrate_Booking_button.setBounds(250,260,140,45);
        administrate_Booking_button.setVerticalTextPosition(SwingConstants.CENTER);
        administrate_Booking_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Fakturera kund button
        // Salehs
        ImageIcon invoiceIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\fakturera.png");
        // Axels
        //ImageIcon invoiceIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\fakturera.png");
        Image invoiceImg = invoiceIcon.getImage();
        Image newInvoiceImg = invoiceImg.getScaledInstance(30,30,java.awt.Image.SCALE_SMOOTH);
        invoiceIcon = new ImageIcon(newInvoiceImg);

        JButton sendInvoice_button = new JButton("<html>Fakturera<br/>Kund</html>",invoiceIcon);
        sendInvoice_button.setBounds(250,180,140,45);
        sendInvoice_button.setVerticalTextPosition(SwingConstants.CENTER);
        sendInvoice_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Logout button
        // Salehs
        ImageIcon logoutIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\logout3.png");
        // Axels
        //ImageIcon logoutIcon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\logout3.png");
        Image logoutImg = logoutIcon.getImage();
        Image newlogoutimg = logoutImg.getScaledInstance(40,30,java.awt.Image.SCALE_SMOOTH);
        logoutIcon = new ImageIcon(newlogoutimg);

        JButton logout_button = new JButton("Logga Ut",logoutIcon);
        logout_button.setBounds(50,260,140,45);
        logout_button.setVerticalTextPosition(SwingConstants.CENTER);
        logout_button.setHorizontalAlignment(SwingConstants.LEFT);

        successful_Admin_loginPage.add(administrate_User_button);
        successful_Admin_loginPage.add(administrate_Cleaner_button);
        successful_Admin_loginPage.add(administrate_Booking_button);
        successful_Admin_loginPage.add(sendInvoice_button);
        successful_Admin_loginPage.add(myPage_Admin_button);
        successful_Admin_loginPage.add(logout_button);
        successful_Admin_loginPage.add(welcomeLabel);

        logout_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                successful_Admin_loginPage.setVisible(false);
                successful_Admin_loginPage.removeAll();
                Main.panel1.setVisible(true);
            }
        });

        administrate_User_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                administrate_user_menu();
                successful_Admin_loginPage.setVisible(false);
                administrateUserMenu_Panel.setVisible(true);
            }
        });
        administrate_Cleaner_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                administrate_cleaner_menu();
                successful_Admin_loginPage.setVisible(false);
                administrateCleanerMenu_Panel.setVisible(true);
            }
        });
        administrate_Booking_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.booking_Change_by_Admin();
            }
        });
        sendInvoice_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.adminSendInvoice();
            }
        });
        myPage_Admin_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               // myPage(username, password);
                connect.myPageInfoUser(username,password,"admin");
            }
        });
    }

    Register register = new Register();
    RegisterCleaner registerCleaner = new RegisterCleaner();


    // Panel och metod för administrera städare sidan
    static JPanel administrateCleanerMenu_Panel = new JPanel();
    public void administrate_cleaner_menu(){
        administrateCleanerMenu_Panel.setLayout(null);
        administrateCleanerMenu_Panel.setBounds(0,10,500,450);
        successful_Admin_loginPage.setVisible(false);
        administrateCleanerMenu_Panel.setVisible(true);

        JLabel administrate_cleaner_label = new JLabel("Administrera Städare");
        administrate_cleaner_label.setBounds(105,0,500,200);
        administrate_cleaner_label.setFont(new Font("Arial",Font.PLAIN,30));

        // skapa städare button
        ImageIcon createCleanerIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\addCleaner2.png");
        Image myCreateCleanerImg = createCleanerIcon.getImage();
        Image newMyCreateCleanerImg = myCreateCleanerImg.getScaledInstance(40,40,java.awt.Image.SCALE_SMOOTH);
        createCleanerIcon = new ImageIcon(newMyCreateCleanerImg);
        JButton createCleaner_button = new JButton("<html>Skapa<br/>Städarkonto</html>",createCleanerIcon);
        createCleaner_button.setBounds(250,200,140,45);
        createCleaner_button.setVerticalTextPosition(SwingConstants.CENTER);
        createCleaner_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Radera städare button
        ImageIcon deleteCleanerIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\removeCleaner2.png");
        Image myDeleteCleaner = deleteCleanerIcon.getImage();
        Image newMyDeleteCleaner = myDeleteCleaner.getScaledInstance(40,40,java.awt.Image.SCALE_SMOOTH);
        deleteCleanerIcon = new ImageIcon(newMyDeleteCleaner);
        JButton deleteCleaner_button = new JButton("<html>Radera<br/>Städarkonto</html>",deleteCleanerIcon);
        deleteCleaner_button.setBounds(100,200,140,45);
        deleteCleaner_button.setVerticalTextPosition(SwingConstants.CENTER);
        deleteCleaner_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Saleh
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axel
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                administrateCleanerMenu_Panel.setVisible(false);
                successful_Admin_loginPage.setVisible(true);
            }
        });
        administrateCleanerMenu_Panel.add(createCleaner_button);
        administrateCleanerMenu_Panel.add(deleteCleaner_button);
        administrateCleanerMenu_Panel.add(back_label);
        administrateCleanerMenu_Panel.add(administrate_cleaner_label);
        createCleaner_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerCleaner.addToRegisterCleanerList("admin");
                RegisterCleaner.cleanerRegister.setBounds(0,10,500,450);
                administrateCleanerMenu_Panel.setVisible(false);
                RegisterCleaner.cleanerRegister.setVisible(true);
            }
        });

        deleteCleaner_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.showCleanerList();
            }
        });
    }


    // Panel och metod för administrera kunder sidan
    static JPanel administrateUserMenu_Panel = new JPanel();
    public void administrate_user_menu(){
        administrateUserMenu_Panel.setLayout(null);
        administrateUserMenu_Panel.setBounds(0,10,500,450);
        successful_Admin_loginPage.setVisible(false);
        administrateUserMenu_Panel.setVisible(true);

        JLabel administrate_user_label = new JLabel("Administrera Kunder");
        administrate_user_label.setBounds(105,0,500,200);
        administrate_user_label.setFont(new Font("Arial",Font.PLAIN,30));

        // skapa kund button
        ImageIcon createUserIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\addUser.png");
        Image myCreateUserImg = createUserIcon.getImage();
        Image newMyCreateUsereImg = myCreateUserImg.getScaledInstance(40,40,java.awt.Image.SCALE_SMOOTH);
        createUserIcon = new ImageIcon(newMyCreateUsereImg);
        JButton createUser_button = new JButton("<html>Skapa<br/>Kundkonto</html>",createUserIcon);
        createUser_button.setBounds(250,200,140,45);
        createUser_button.setVerticalTextPosition(SwingConstants.CENTER);
        createUser_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Radera kund button
        ImageIcon deleteUserIcon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\removeUser2.png");
        Image myDeleteUser = deleteUserIcon.getImage();
        Image newMyDeleteUser = myDeleteUser.getScaledInstance(40,40,java.awt.Image.SCALE_SMOOTH);
        deleteUserIcon = new ImageIcon(newMyDeleteUser);
        JButton deleteUser_button = new JButton("<html>Radera<br/>Kundkonto</html>",deleteUserIcon);
        deleteUser_button.setBounds(100,200,140,45);
        deleteUser_button.setVerticalTextPosition(SwingConstants.CENTER);
        deleteUser_button.setHorizontalAlignment(SwingConstants.LEFT);

        // Saleh
        ImageIcon icon = new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\backButton1.png");
        // Axel
        //ImageIcon icon = new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\backButton1.png");
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(50,40,java.awt.Image.SCALE_SMOOTH);
        icon = new ImageIcon(newimg);

        JLabel back_label = new JLabel(icon);
        back_label.setBounds(0,0,50,40);
        back_label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                administrateUserMenu_Panel.setVisible(false);
                successful_Admin_loginPage.setVisible(true);
            }
        });

        createUser_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                register.addToRegisterList("admin");
                Register.panel3.setBounds(0,10,500,450);
                administrateUserMenu_Panel.setVisible(false);
                Register.panel3.setVisible(true);
            }
        });
        deleteUser_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                connect.showUserList();
            }
        });
        administrateUserMenu_Panel.add(createUser_button);
        administrateUserMenu_Panel.add(deleteUser_button);
        administrateUserMenu_Panel.add(back_label);
        administrateUserMenu_Panel.add(administrate_user_label);
    }
}
