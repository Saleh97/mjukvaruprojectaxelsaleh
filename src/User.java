import java.util.Arrays;
import java.util.UUID;

public class User {

    private int id;
    private String name;
    private String password;
    private String adress;
    private String[] bookings;
    private String userKind;

    public User(String name, String password, String adress, String userKind) {
        this.name = name;
        this.password = password;
        this.adress = adress;
        this.userKind = userKind;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String[] getBookings() {
        return bookings;
    }

    public void setBookings(String[] bookings) {
        this.bookings = bookings;
    }
    public String getUserKind() {
        return userKind;
    }

    public void setUserKind(String userKind) {
        this.userKind = userKind;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", adress='" + adress + '\'' +
                ", bookings=" + Arrays.toString(bookings) +
                '}';
    }
}
