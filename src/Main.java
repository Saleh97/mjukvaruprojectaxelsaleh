import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Main {

    // Skapandet av fönstret och namnet
    static JFrame f = new JFrame("Städafint AB");
    static JPanel panel1 = new JPanel();
    static JPanel panel2 = new JPanel();
    static JPanel cleanerStartPanel = new JPanel();
    static JPanel adminStartPanel = new JPanel();

    public static void main(String[] args) {
        panel1.setBounds(0,0,500,450);
        panel2.setBounds(40,150,200,400);
        panel2.setVisible(false);
        adminStartPanel.setBounds(40,150,200,400);
        adminStartPanel.setVisible(false);
        cleanerStartPanel.setVisible(false);
        f.add(panel1, BorderLayout.CENTER);

        Register registerClass = new Register();
        RegisterCleaner registerCleaner = new RegisterCleaner();
        RegisterAdmin registerAdmin = new RegisterAdmin();
        registerAdmin.addToRegisterAdminList();
        registerCleaner.logInInput();

        // Lägger upp start panelen (panel 1)
        JLabel whoAreYou = new JLabel("Välj vem du är");
        whoAreYou.setBounds(50,20,300,30);
        whoAreYou.setPreferredSize(new Dimension(100,30));

        JButton customer = new JButton("Kund");
        customer.setBounds(50,50,100,30);
        customer.setPreferredSize(new Dimension(100,30));

        JButton cleaner = new JButton("Städare");
        cleaner.setBounds(50,100,100,30);
        cleaner.setPreferredSize(new Dimension(100,30));

        JButton admin = new JButton("Admin");
        admin.setBounds(50,150,100,30);
        admin.setPreferredSize(new Dimension(100,30));
        // Salehs
        panel1.add(new JLabel(new ImageIcon("E:\\EcUtbildning\\Mjukvaruprojekt_Portfoile\\Uppgift\\Image\\StartLogga.jpg")));
        // Axels
        //panel1.add(new JLabel(new ImageIcon("C:\\Users\\axelc\\IdeaProjects\\mjukvaruprojectaxelsaleh\\Image\\StartLogga.jpg")));

        // Lägger till knappar m.m. till panelen
        panel1.add(whoAreYou);
        panel1.add(customer);
        panel1.add(cleaner);
        panel1.add(admin);

        // Sätter properties på framen
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(500,450);
        f.setLayout(null);
        f.setVisible(true);
        Main.f.setMaximumSize(new Dimension(400, 400));
        Main.f.setMinimumSize(new Dimension(200, 100));
        Main.f.setLocationRelativeTo(null);

        // Metoder för knapparna
        customer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel1.setVisible(false);
                Register.userlogInPanel.setBounds(0,10,500,450);
                Register.userlogInPanel.setVisible(true);
            }
        });
        cleaner.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel1.setVisible(false);
                //cleanerStartPanel.setVisible(true);
                RegisterCleaner.cleanerLoginPanel.setBounds(0,10,500,450);
                RegisterCleaner.cleanerLoginPanel.setVisible(true);
            }
        });
        admin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel1.setVisible(false);
                RegisterAdmin.adminLogin_Panel.setVisible(true);
                RegisterAdmin.adminLogin_Panel.setBounds(0,10,500,450);
            }
        });
        registerAdmin.logInInput();

        //Kund register & Login
        JButton register = new JButton("Registrera");
        JButton login = new JButton("Logga in");
        register.setBounds(50,50,95,30);
        login.setBounds(50,100,95,30);
        JButton userMenuBack = new JButton("Tillbaka");
        userMenuBack.setBounds(10,10,95,30);
        panel2.add(userMenuBack);
        panel2.add(register);
        panel2.add(login);
        register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerClass.addToRegisterList("kund");
                panel2.setVisible(false);
                Register.panel3.setBounds(0,10,500,450);
                Register.panel3.setVisible(true);
            }
        });

        registerClass.logInInput();
        login.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel2.setVisible(false);
                Register.userlogInPanel.setBounds(0,10,150,500);
                Register.userlogInPanel.setVisible(true);
            }
        });
        userMenuBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panel2.setVisible(false);
                panel1.setVisible(true);
            }
        });

        //Städare register & Login
       /* cleanerStartPanel.setBounds(40,150,200,400);
        JButton cleanerRegisterbtn = new JButton("Registrera");
        JButton cleanerLoginbtn = new JButton("Logga in");
        JButton cleanerBack = new JButton("Tillbaka");
        cleanerRegisterbtn.setBounds(50,50,95,30);
        cleanerLoginbtn.setBounds(50,100,95,30);
        //cleanerStartPanel.add(cleanerRegisterbtn);
        cleanerStartPanel.add(cleanerLoginbtn);
        cleanerStartPanel.add(cleanerBack);

        cleanerBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cleanerStartPanel.setVisible(false);
                panel1.setVisible(true);
            }
        });

        cleanerRegisterbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cleanerStartPanel.setVisible(false);
                //f.remove(cleanerStartPanel);
                registerCleaner.addToRegisterCleanerList("cleaner");
                RegisterCleaner.cleanerRegister.setBounds(40,10,150,500);
                RegisterCleaner.cleanerRegister.setVisible(true);
            }
        });

        cleanerLoginbtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cleanerStartPanel.setVisible(false);
                RegisterCleaner.cleanerLoginPanel.setBounds(40,10,150,500);
                RegisterCleaner.cleanerLoginPanel.setVisible(true);
            }
        });

        //Admin Register & Login
        JButton adminRegisterBtn = new JButton("Registrera");
        JButton adminLoginBtn = new JButton("Logga in");
        JButton adminMenuBackBtn = new JButton("Tillbaka");
        adminRegisterBtn.setBounds(50,50,95,30);
        adminLoginBtn.setBounds(50,100,95,30);
        adminMenuBackBtn.setBounds(50,150,95,30);
        adminStartPanel.add(adminMenuBackBtn);
        //adminStartPanel.add(adminRegisterBtn);
        adminStartPanel.add(adminLoginBtn);
        adminRegisterBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adminStartPanel.setVisible(false);
                RegisterAdmin.adminRegister_Panel.setBounds(40,10,150,500);
                RegisterAdmin.adminRegister_Panel.setVisible(true);
            }
        });

        registerAdmin.logInInput();
        adminLoginBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adminStartPanel.setVisible(false);
                RegisterAdmin.adminLogin_Panel.setBounds(40,10,150,500);
                RegisterAdmin.adminLogin_Panel.setVisible(true);
            }
        });

        adminMenuBackBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adminStartPanel.setVisible(false);
                panel1.setVisible(true);
            }
        });*/

        //Alla Jpanel som har lagts till i Jframe
        f.add(RegisterCleaner.cleanerLoginPanel, BorderLayout.CENTER);
        f.add(Register.userlogInPanel, BorderLayout.CENTER);
        f.add(RegisterAdmin.adminRegister_Panel,BorderLayout.CENTER);
        f.add(RegisterAdmin.adminLogin_Panel);
        f.add(panel2, BorderLayout.CENTER);
        f.add(Register.panel3);
        f.add(RegisterCleaner.cleanerRegister, BorderLayout.CENTER);
        f.add(RegisterCleaner.successful_cleaner_loginPage, BorderLayout.CENTER);
        f.add(Connect.cleanerConfirmedBookingsPanel, BorderLayout.CENTER);
        f.add(Connect.cleanerDoneBookingsPanel, BorderLayout.CENTER);
        f.add(Connect.cleanerAssignedBookingPanel, BorderLayout.CENTER);
        f.add(Register.successful_customer_loginPage, BorderLayout.CENTER);
        f.add(Connect.showAllUserBookings_Panel, BorderLayout.CENTER);
        f.add(Connect.showAllDoneUserBookings_Panel, BorderLayout.CENTER);
        f.add(Connect.removeUserBooking_Panel, BorderLayout.CENTER);
        f.add(CreateBooking.addBoking_Panel);
        f.add(RegisterAdmin.successful_Admin_loginPage);
        f.add(RegisterAdmin.administrateUserMenu_Panel);
        f.add(RegisterAdmin.administrateCleanerMenu_Panel);
        f.add(Connect.showUserList_Panel);
        f.add(Connect.showCleanerList_Panel);
        f.add(Connect.administrate_Booking_Panel);
        f.add(Connect.specificBooking_Panel);
        f.add(Connect.adminInvoice_Panel);
        f.add(Connect.payInvoice_Panel);
        f.add(Connect.userMyPage_panel);
        f.add(Connect.showAllUserBookingTOP_TOPPanel);
        f.add(Connect.showAllDoneUserBookingTOP_TOPPanel);
        f.add(Connect.removeUserTOPBooking_TOPPanel);
        f.add(Connect.payInvoiceTOP_TOPPanel);
        f.add(Connect.cleanerAssignedBookingTOP_TOPPanel);
        f.add(Connect.cleanerConfirmedTOPBooking_TOPPanel);
        f.add(Connect.cleanerDoneBookingsTOP_TOPPanel);
        f.add(Connect.showUserTopList_TopPanel);
        f.add(Connect.ShowCleanerTOPList_TopPanel);
        f.add(Connect.administrate_TopBooking_TopPanel);
        f.add(Connect.adminInvoiceTop_TopPanel);
    }
}
